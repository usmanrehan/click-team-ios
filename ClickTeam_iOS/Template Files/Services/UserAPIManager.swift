import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class UsersAPIManager: APIManagerBase {
    //MARK:- /LoginEmployee
    func LoginEmployee(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Route.Login.rawValue, params: params)! as URL
        print(route)
        Utility.showLoader()
        self.postMultipartDictionaryResponseWith(route: route, parameters: params, success: success, failure: failure, withHeader: false)
    }
    //MARK:- /CMS
    func getCMS(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Route.Pages.rawValue, params: params)! as URL
        print(route)
        Utility.showLoader()
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: true)
    }
    //MARK:- /LoginAdmin
    func LoginAdmin(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Route.Login.rawValue, params: params)! as URL
        print(route)
        Utility.showLoader()
        self.postDictionaryResponseWith(route: route, parameters: params, success: success, failure: failure, withHeader: false)
    }
    //MARK:- /GetAllEmployees
    func GetAllEmployees(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Route.Users.rawValue, params: params)! as URL
        print(route)
        Utility.showLoader()
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: true)
    }
    //MARK:- /GetAllJobs
    func GetAllJobs(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Route.Tasks.rawValue, params: params)! as URL
        print(route)
        Utility.showLoader()
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: true)
    }
    //MARK:- /GetNotifications
    func GetNotifications(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Route.notifications.rawValue, params: params)! as URL
        print(route)
        Utility.showLoader()
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: true)
    }
    //MARK:- /GetEarningReport
    func GetEarningReport(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Route.monthlypays.rawValue, params: params)! as URL
        print(route)
        Utility.showLoader()
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: true)
    }
    //MARK:- /GetAppraisalReport
    func GetAppraisalReport(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Route.apprasals.rawValue, params: params)! as URL
        print(route)
        Utility.showLoader()
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: true)
    }
    //MARK:- /GetComparasion
    func GetComparasion(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Route.comparison.rawValue, params: params)! as URL
        print(route)
        Utility.showLoader()
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: true)
    }
    //MARK:- /GetRoles
    func GetRoles(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Route.roles.rawValue, params: params)! as URL
        print(route)
        Utility.showLoader()
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: true)
    }
    //MARK:- /SetDeductions
    func SetDeductions(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Route.deductions.rawValue)! as URL
        Utility.showLoader()
        self.postDictionaryResponseWith(route: route, parameters: params, success: success, failure: failure, withHeader: true)
    }
    //MARK:- /AddComplain
    func AddComplain(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Route.usercomplains.rawValue)! as URL
        Utility.showLoader()
        self.postMultipartDictionaryResponseWith(route: route, parameters: params, success: success, failure: failure, withHeader: true)
    }
    //MARK:- /UpdatePerformance
    func UpdatePerformance(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure,id:String) {
        let route: URL = POSTURLforRoute(route: Route.taskCategories.rawValue + id)! as URL
        print(route)
        Utility.showLoader()
        self.putDictionaryResponseWith(route: route, parameters: params, success: success, failure: failure, withHeader: true)
    }
    //MARK:- /GetReminders
    func GetReminders(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Route.reminders.rawValue, params: params)! as URL
        print(route)
        Utility.showLoader()
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: true)
    }
    //MARK:- /SetReminder
    func SetReminder(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Route.reminders.rawValue)! as URL
        Utility.showLoader()
        self.postDictionaryResponseWith(route: route, parameters: params, success: success, failure: failure, withHeader: true)
    }
    //MARK:- /assignTask
    func AssignUnAssignTask(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Route.assignTask.rawValue)! as URL
        Utility.showLoader()
        self.postDictionaryResponseWith(route: route, parameters: params, success: success, failure: failure, withHeader: true)
    }
    //MARK:- /sendMessage
    func sendMessage(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Route.contactus.rawValue)! as URL
        print(route)
        Utility.showLoader()
        self.postDictionaryResponseWith(route: route, parameters: params, success: success, failure: failure, withHeader: true)
    }
    //MARK:- /updateRole
    func updateRole(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Route.updateRole.rawValue)! as URL
        print(route)
        Utility.showLoader()
        self.postDictionaryResponseWith(route: route, parameters: params, success: success, failure: failure, withHeader: true)
    }
    //MARK:- /UpdatePerformance
    func UpdateUser(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure,id:String) {
        let route: URL = POSTURLforRoute(route: Route.updateUser.rawValue + id)! as URL
        print(route)
        Utility.showLoader()
        self.putDictionaryResponseWith(route: route, parameters: params, success: success, failure: failure, withHeader: true)
    }
    //MARK:- /taskComplete
    func taskComplete(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Route.taskComplete.rawValue)! as URL
        print(route)
        Utility.showLoader()
        self.postDictionaryResponseWith(route: route, parameters: params, success: success, failure: failure, withHeader: true)
    }
    //MARK:- /setAppraisal
    func setAppraisal(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Route.apprasals.rawValue)! as URL
        print(route)
        Utility.showLoader()
        self.postDictionaryResponseWith(route: route, parameters: params, success: success, failure: failure, withHeader: true)
    }
    //MARK:- /addTaskWithCategory
    func addTaskWithCategory(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Route.taskwithcat.rawValue)! as URL
        print(route)
        Utility.showLoader()
        self.postDictionaryResponseWith(route: route, parameters: params, success: success, failure: failure, withHeader: true)
    }
    //MARK:- /GetTaskCategories
    func GetTaskCategories(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Route.task_Categories.rawValue, params: params)! as URL
        print(route)
        Utility.showLoader()
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: true)
    }
}
//MARK:- User App Apis
extension UsersAPIManager{
    //MARK:- /GetAttendances
    func GetAttendances(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Route.Attendances.rawValue, params: params)! as URL
        print(route)
        Utility.showLoader()
        self.getDictionaryResponseWith(route: route, success: success, failure: failure, withHeader: true)
    }
    //MARK:- /Logout
    func Logout(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Route.Logout.rawValue, params: params)! as URL
        print(route)
        Utility.showLoader()
        self.postArrayResponseWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func GetAdminInfo(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Route.adminconfigurations.rawValue, params: params)! as URL
        print(route)
        Utility.showLoader()
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: false)
    }
    //MARK:- /PostCarriedOutYesNo
    func PostCarriedOutYesNo(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Route.taskComments.rawValue)! as URL
        print(route)
        Utility.showLoader()
        self.postDictionaryResponseWith(route: route, parameters: params, success: success, failure: failure, withHeader: true)
    }
    //MARK:- /AddTaskWithCategoryID
    func AddTaskWithCategoryID(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Route.Tasks.rawValue)! as URL
        print(route)
        Utility.showLoader()
        self.postDictionaryResponseWith(route: route, parameters: params, success: success, failure: failure, withHeader: true)
    }
    //MARK:- /GetMessages
    func GetMessages(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Route.contactus.rawValue, params: params)! as URL
        print(route)
        Utility.showLoader()
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: true)
    }
    //MARK:- /GetComplains
    func GetComplains(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Route.usercomplains.rawValue, params: params)! as URL
        print(route)
        Utility.showLoader()
        self.getArrayResponseWith(route: route, success: success, failure: failure, withHeader: true)
    }
    //MARK:- /PostTaskwithCatComment
    func PostTaskwithCatComment(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Route.taskwithcatcomment.rawValue)! as URL
        print(route)
        Utility.showLoader()
        self.postMultipartDictionaryResponseWith(route: route, parameters: params, success: success, failure: failure, withHeader: true)
    }
}


