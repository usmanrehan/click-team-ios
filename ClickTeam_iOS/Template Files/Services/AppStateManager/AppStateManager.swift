import Foundation
import UIKit
import RealmSwift

class AppStateManager: NSObject {
    
    static let sharedInstance = AppStateManager()
    var loggedInUser: User!
    var realm: Realm!
    
    override init() {
        super.init()
        if(!(realm != nil)){
            realm = try! Realm()
        }
        loggedInUser = realm.objects(User.self).first
    }
    
    func isUserLoggedIn() -> Bool{
        if (self.loggedInUser) != nil {
            if self.loggedInUser.isInvalidated {
                return false
            }
            return true
        }
        return false
    }
    
    func loginUser(user: User){
        self.loggedInUser = user
        try! Global.APP_REALM?.write(){
            Global.APP_REALM?.add(self.loggedInUser, update: true)
        }
        AppDelegate.shared.changeRootViewController()
    }
    
    func logoutUser(){
        Utility.main.showAlert(message: Strings.ASK_LOGOUT.text, title: Strings.LOGOUT.text, controller: Utility.main.topViewController()!) { (yes, no) in
            if yes != nil{
                self.processLogout()
            }
        }
    }
}
extension AppStateManager{
    private func processLogout(){
        APIManager.sharedInstance.usersAPIManager.Logout(params: [:], success: { (responseObject) in
            print(responseObject)
            try! Global.APP_REALM?.write() {
                Global.APP_REALM?.delete(self.loggedInUser)
                self.loggedInUser = nil
            }
            AppDelegate.shared.changeRootViewController()
        }) { (error) in
            print(error)
        }
    }
}
