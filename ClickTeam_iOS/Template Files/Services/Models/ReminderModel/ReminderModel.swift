//
//  ReminderModel.swift
//
//  Created by Hamza Hasan on 17/06/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class ReminderModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updated_at"
    static let id = "id"
    static let reminderTitle = "reminder_title"
    static let createdAt = "created_at"
    static let reminderDate = "reminder_date"
    static let userId = "user_id"
    static let reminderDescription = "reminder_description"
  }

  // MARK: Properties
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var id = 0
  @objc dynamic var reminderTitle: String? = ""
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var reminderDate: String? = ""
  @objc dynamic var userId: String? = ""
  @objc dynamic var reminderDescription: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    updatedAt <- map[SerializationKeys.updatedAt]
    id <- map[SerializationKeys.id]
    reminderTitle <- map[SerializationKeys.reminderTitle]
    createdAt <- map[SerializationKeys.createdAt]
    reminderDate <- map[SerializationKeys.reminderDate]
    userId <- map[SerializationKeys.userId]
    reminderDescription <- map[SerializationKeys.reminderDescription]
  }


}
