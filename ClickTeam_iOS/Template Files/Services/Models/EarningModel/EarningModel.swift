//
//  EarningModel.swift
//
//  Created by Hamza Hasan on 14/06/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class EarningModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let other = "other"
    static let updatedAt = "updated_at"
    static let totals = "totals"
    static let confromanceCash = "confromance_cash"
    static let lates = "lates"
    static let lossCauseCompany = "loss_cause_company"
    static let salary = "salary"
    static let hoursWorked = "hours_worked"
    static let incomeTax = "income_tax"
    static let ssnit = "ssnit"
    static let id = "id"
    static let conformanceEarned = "conformance_earned"
    static let createdAt = "created_at"
    static let performanceCash = "performance_cash"
    static let cashValue = "cash_value"
    static let month = "month"
    static let userId = "user_id"
    static let subtotals = "subtotals"
    static let performanceEarned = "performance_earned"
  }

  // MARK: Properties
  @objc dynamic var other = 0
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var totals: String? = ""
  @objc dynamic var confromanceCash = 0
  @objc dynamic var lates = 0
  @objc dynamic var lossCauseCompany = 0
  @objc dynamic var salary: String? = ""
  @objc dynamic var hoursWorked: String? = ""
  @objc dynamic var incomeTax: String? = ""
  @objc dynamic var ssnit = 0
  @objc dynamic var id = 0
  @objc dynamic var conformanceEarned = 0
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var performanceCash = 0
  @objc dynamic var cashValue = 0
  @objc dynamic var month: String? = ""
  @objc dynamic var userId: String? = ""
  @objc dynamic var subtotals: String? = ""
  @objc dynamic var performanceEarned = 0

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    other <- map[SerializationKeys.other]
    updatedAt <- map[SerializationKeys.updatedAt]
    totals <- map[SerializationKeys.totals]
    confromanceCash <- map[SerializationKeys.confromanceCash]
    lates <- map[SerializationKeys.lates]
    lossCauseCompany <- map[SerializationKeys.lossCauseCompany]
    salary <- map[SerializationKeys.salary]
    hoursWorked <- map[SerializationKeys.hoursWorked]
    incomeTax <- map[SerializationKeys.incomeTax]
    ssnit <- map[SerializationKeys.ssnit]
    id <- map[SerializationKeys.id]
    conformanceEarned <- map[SerializationKeys.conformanceEarned]
    createdAt <- map[SerializationKeys.createdAt]
    performanceCash <- map[SerializationKeys.performanceCash]
    cashValue <- map[SerializationKeys.cashValue]
    month <- map[SerializationKeys.month]
    userId <- map[SerializationKeys.userId]
    subtotals <- map[SerializationKeys.subtotals]
    performanceEarned <- map[SerializationKeys.performanceEarned]
  }


}
