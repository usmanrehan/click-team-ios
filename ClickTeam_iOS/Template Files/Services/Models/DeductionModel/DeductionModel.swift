//
//  DeductionModel.swift
//
//  Created by Hamza Hasan on 15/06/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class DeductionModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let roleId = "role_id"
    static let updatedAt = "updated_at"
    static let id = "id"
    static let lossCaused = "loss_caused"
    static let createdAt = "created_at"
    static let ssnit = "ssnit"
    static let incometax = "incometax"
  }

  // MARK: Properties
  @objc dynamic var roleId: String? = ""
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var id = 0
  @objc dynamic var lossCaused: String? = ""
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var ssnit: String? = ""
  @objc dynamic var incometax: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    roleId <- map[SerializationKeys.roleId]
    updatedAt <- map[SerializationKeys.updatedAt]
    id <- map[SerializationKeys.id]
    lossCaused <- map[SerializationKeys.lossCaused]
    createdAt <- map[SerializationKeys.createdAt]
    ssnit <- map[SerializationKeys.ssnit]
    incometax <- map[SerializationKeys.incometax]
  }


}
