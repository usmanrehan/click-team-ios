//
//  AttendanceModel.swift
//
//  Created by Hamza Hasan on 21/06/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class AttendanceModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let overtime = "overtime"
    static let workedHours = "worked_hours"
    static let workingHours = "working_hours"
    static let workedDays = "worked_days"
    static let lates = "lates"
    static let id = "id"
  }

  // MARK: Properties
  @objc dynamic var overtime = 0
  @objc dynamic var workedHours: String? = ""
  @objc dynamic var workingHours: String? = ""
  @objc dynamic var workedDays: String? = ""
  @objc dynamic var lates = 0
  @objc dynamic var id = 0

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    overtime <- map[SerializationKeys.overtime]
    workedHours <- map[SerializationKeys.workedHours]
    workingHours <- map[SerializationKeys.workingHours]
    workedDays <- map[SerializationKeys.workedDays]
    lates <- map[SerializationKeys.lates]
    id <- map[SerializationKeys.id]
  }


}
