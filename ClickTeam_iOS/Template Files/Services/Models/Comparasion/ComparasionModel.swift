//
//  ComparasionModel.swift
//
//  Created by Hamza Hasan on 14/06/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class ComparasionModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let performancePoints = "performance_points"
    static let name = "name"
    static let email = "email"
    static let details = "details"
    static let eom = "eom"
    static let id = "id"
    static let roles = "roles"
    static let workingHours = "Working_hours"
    static let createdAt = "created_at"
    static let ratings = "ratings"
    static let attendance = "Attendance"
    static let protect = "protect"
    static let conformancePoints = "conformance_points"
  }

  // MARK: Properties
  @objc dynamic var performancePoints = 0
  @objc dynamic var name: String? = ""
  @objc dynamic var email: String? = ""
  @objc dynamic var details: Details?
  @objc dynamic var eom = false
  @objc dynamic var id = 0
  @objc dynamic var roles: String? = ""
  @objc dynamic var workingHours: String? = ""
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var ratings = 0
  @objc dynamic var attendance = 0
  @objc dynamic var protect: String? = ""
  @objc dynamic var conformancePoints = 0

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    performancePoints <- map[SerializationKeys.performancePoints]
    name <- map[SerializationKeys.name]
    email <- map[SerializationKeys.email]
    details <- map[SerializationKeys.details]
    eom <- map[SerializationKeys.eom]
    id <- map[SerializationKeys.id]
    roles <- map[SerializationKeys.roles]
    workingHours <- map[SerializationKeys.workingHours]
    createdAt <- map[SerializationKeys.createdAt]
    ratings <- map[SerializationKeys.ratings]
    attendance <- map[SerializationKeys.attendance]
    protect <- map[SerializationKeys.protect]
    conformancePoints <- map[SerializationKeys.conformancePoints]
  }


}
