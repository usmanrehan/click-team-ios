//
//  NotificationModel.swift
//
//  Created by Hamza Hasan on 11/06/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class NotificationModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let refId = "ref_id"
    static let status = "status"
    static let senderId = "sender_id"
    static let updatedAt = "updated_at"
    static let id = "id"
    static let actionType = "action_type"
    static let createdAt = "created_at"
    static let message = "message"
    static let url = "url"
  }

  // MARK: Properties
  @objc dynamic var refId: String? = ""
  @objc dynamic var status = false
  @objc dynamic var senderId: String? = ""
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var id = 0
  @objc dynamic var actionType: String? = ""
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var message: String? = ""
  @objc dynamic var url: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    refId <- map[SerializationKeys.refId]
    status <- map[SerializationKeys.status]
    senderId <- map[SerializationKeys.senderId]
    updatedAt <- map[SerializationKeys.updatedAt]
    id <- map[SerializationKeys.id]
    actionType <- map[SerializationKeys.actionType]
    createdAt <- map[SerializationKeys.createdAt]
    message <- map[SerializationKeys.message]
    url <- map[SerializationKeys.url]
  }


}
