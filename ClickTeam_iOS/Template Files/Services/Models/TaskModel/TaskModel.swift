//
//  TaskModel.swift
//
//  Created by Hamza Hasan on 15/06/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class TaskModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let id = "id"
    static let title = "title"
    static let cashValue = "cash_value"
    static let roles = "roles"
    static let parentTaskId = "parent_task_id"
    static let earnPoint = "earn_point"
  }

  // MARK: Properties
  @objc dynamic var id: String? = ""
  @objc dynamic var title: String? = ""
  @objc dynamic var cashValue: String? = ""
  @objc dynamic var roles: String? = ""
  @objc dynamic var parentTaskId: String? = ""
  @objc dynamic var earnPoint: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    id <- map[SerializationKeys.id]
    title <- map[SerializationKeys.title]
    cashValue <- map[SerializationKeys.cashValue]
    roles <- map[SerializationKeys.roles]
    parentTaskId <- map[SerializationKeys.parentTaskId]
    earnPoint <- map[SerializationKeys.earnPoint]
  }


}
