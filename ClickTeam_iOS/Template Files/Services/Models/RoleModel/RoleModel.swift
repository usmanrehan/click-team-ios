//
//  RoleModel.swift
//
//  Created by Hamza Hasan on 15/06/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class RoleModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let name = "name"
    static let isProtected = "is_protected"
    static let updatedAt = "updated_at"
    static let id = "id"
    static let displayName = "display_name"
    static let workingHours = "Working_hours"
    static let createdAt = "created_at"
    static let descriptionValue = "description"
    static let cashValue = "cash_value"
    static let tax = "tax"
  }

  // MARK: Properties
  @objc dynamic var name: String? = ""
  @objc dynamic var isProtected: String? = ""
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var id = 0
  @objc dynamic var displayName: String? = ""
  @objc dynamic var workingHours: String? = ""
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var descriptionValue: String? = ""
  @objc dynamic var cashValue: String? = ""
  @objc dynamic var tax: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    name <- map[SerializationKeys.name]
    isProtected <- map[SerializationKeys.isProtected]
    updatedAt <- map[SerializationKeys.updatedAt]
    id <- map[SerializationKeys.id]
    displayName <- map[SerializationKeys.displayName]
    workingHours <- map[SerializationKeys.workingHours]
    createdAt <- map[SerializationKeys.createdAt]
    descriptionValue <- map[SerializationKeys.descriptionValue]
    cashValue <- map[SerializationKeys.cashValue]
    tax <- map[SerializationKeys.tax]
  }


}
