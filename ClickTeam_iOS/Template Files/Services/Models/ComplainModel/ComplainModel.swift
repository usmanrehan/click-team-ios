//
//  ComplainModel.swift
//
//  Created by Hamza Hasan on 22/06/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class ComplainModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let complainComment = "complain_comment"
    static let updatedAt = "updated_at"
    static let id = "id"
    static let complain = "complain"
    static let createdAt = "created_at"
    static let images = "images"
    static let userId = "user_id"
    static let complainRate = "complain_rate"
  }

  // MARK: Properties
  @objc dynamic var complainComment: String? = ""
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var id = 0
  @objc dynamic var complain: String? = ""
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var images: String? = ""
  @objc dynamic var userId: String? = ""
  @objc dynamic var complainRate: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    complainComment <- map[SerializationKeys.complainComment]
    updatedAt <- map[SerializationKeys.updatedAt]
    id <- map[SerializationKeys.id]
    complain <- map[SerializationKeys.complain]
    createdAt <- map[SerializationKeys.createdAt]
    images <- map[SerializationKeys.images]
    userId <- map[SerializationKeys.userId]
    complainRate <- map[SerializationKeys.complainRate]
  }


}
