//
//  User.swift
//
//  Created by Hamza Hasan on 07/06/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class User: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let name = "name"
    static let email = "email"
    static let id = "id"
    static let roles = "roles"
    static let ratings = "ratings"
    static let createdAt = "created_at"
    static let details = "details"
    static let protect = "protect"
    static let tokenType = "token_type"
    static let accessToken = "access_token"
    static let expiresIn = "expires_in"
  }

  // MARK: Properties
  @objc dynamic var name: String? = ""
  @objc dynamic var email: String? = ""
  @objc dynamic var id = 0
  var roles = List<Roles>()
  @objc dynamic var ratings: Ratings?
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var details: Details?
  @objc dynamic var protect: String? = ""
  @objc dynamic var tokenType: String? = ""
  @objc dynamic var accessToken: String? = ""
  @objc dynamic var expiresIn = 0

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    name <- map[SerializationKeys.name]
    email <- map[SerializationKeys.email]
    id <- map[SerializationKeys.id]
    roles <- (map[SerializationKeys.roles], ListTransform<Roles>())
    ratings <- map[SerializationKeys.ratings]
    createdAt <- map[SerializationKeys.createdAt]
    details <- map[SerializationKeys.details]
    protect <- map[SerializationKeys.protect]
    tokenType <- map[SerializationKeys.tokenType]
    accessToken <- map[SerializationKeys.accessToken]
    expiresIn <- map[SerializationKeys.expiresIn]
  }


}
