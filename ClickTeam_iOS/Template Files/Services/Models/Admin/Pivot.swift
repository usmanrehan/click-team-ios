//
//  Pivot.swift
//
//  Created by Hamza Hasan on 07/06/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class Pivot: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let roleId = "role_id"
    static let userId = "user_id"
  }

  // MARK: Properties
  @objc dynamic var roleId: String? = ""
  @objc dynamic var userId: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "userId"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    roleId <- map[SerializationKeys.roleId]
    userId <- map[SerializationKeys.userId]
  }


}
