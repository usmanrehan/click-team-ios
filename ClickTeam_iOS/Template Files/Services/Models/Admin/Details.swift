//
//  Details.swift
//
//  Created by Hamza Hasan on 07/06/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class Details: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let lastName = "last_name"
    static let id = "id"
    static let isSocialLogin = "is_social_login"
    static let address = "address"
    static let phone = "phone"
    static let emailUpdates = "email_updates"
    static let firstName = "first_name"
    static let salary = "salary"
    static let isVerified = "is_verified"
    static let image = "image"
  }

  // MARK: Properties
  @objc dynamic var lastName: String? = ""
  @objc dynamic var id = 0
  @objc dynamic var isSocialLogin: String? = ""
  @objc dynamic var address: String? = ""
  @objc dynamic var phone: String? = ""
  @objc dynamic var emailUpdates: String? = ""
  @objc dynamic var firstName: String? = ""
  @objc dynamic var salary: String? = ""
  @objc dynamic var isVerified: String? = ""
  @objc dynamic var image: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    lastName <- map[SerializationKeys.lastName]
    id <- map[SerializationKeys.id]
    isSocialLogin <- map[SerializationKeys.isSocialLogin]
    address <- map[SerializationKeys.address]
    phone <- map[SerializationKeys.phone]
    emailUpdates <- map[SerializationKeys.emailUpdates]
    firstName <- map[SerializationKeys.firstName]
    salary <- map[SerializationKeys.salary]
    isVerified <- map[SerializationKeys.isVerified]
    image <- map[SerializationKeys.image]
  }


}
