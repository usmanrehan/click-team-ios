//
//  Ratings.swift
//
//  Created by Hamza Hasan on 07/06/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class Ratings: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let customerServices = "customer_services"
    static let updatedAt = "updated_at"
    static let jobKnowledge = "job_knowledge"
    static let initiation = "initiation"
    static let id = "id"
    static let punctuality = "punctuality"
    static let createdAt = "created_at"
    static let respect = "respect"
    static let month = "month"
    static let teamwork = "teamwork"
    static let userId = "user_id"
    static let jobAttitude = "job_attitude"
    static let other = "other"
  }

  // MARK: Properties
  @objc dynamic var customerServices = 0
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var jobKnowledge = 0
  @objc dynamic var initiation = 0
  @objc dynamic var id = 0
  @objc dynamic var punctuality = 0
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var respect = 0
  @objc dynamic var month: String? = ""
  @objc dynamic var teamwork = 0
  @objc dynamic var userId: String? = ""
  @objc dynamic var jobAttitude = 0
  @objc dynamic var other = 0

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    customerServices <- map[SerializationKeys.customerServices]
    updatedAt <- map[SerializationKeys.updatedAt]
    jobKnowledge <- map[SerializationKeys.jobKnowledge]
    initiation <- map[SerializationKeys.initiation]
    id <- map[SerializationKeys.id]
    punctuality <- map[SerializationKeys.punctuality]
    createdAt <- map[SerializationKeys.createdAt]
    respect <- map[SerializationKeys.respect]
    month <- map[SerializationKeys.month]
    teamwork <- map[SerializationKeys.teamwork]
    userId <- map[SerializationKeys.userId]
    jobAttitude <- map[SerializationKeys.jobAttitude]
    other <- map[SerializationKeys.other]
  }


}
