//
//  Translations.swift
//
//  Created by Hamza Hasan on 09/06/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class Translations: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let content = "content"
    static let status = "status"
    static let locale = "locale"
    static let updatedAt = "updated_at"
    static let id = "id"
    static let createdAt = "created_at"
    static let title = "title"
    static let pageId = "page_id"
  }

  // MARK: Properties
  @objc dynamic var content: String? = ""
  @objc dynamic var status = false
  @objc dynamic var locale: String? = ""
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var id = 0
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var title: String? = ""
  @objc dynamic var pageId: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    content <- map[SerializationKeys.content]
    status <- map[SerializationKeys.status]
    locale <- map[SerializationKeys.locale]
    updatedAt <- map[SerializationKeys.updatedAt]
    id <- map[SerializationKeys.id]
    createdAt <- map[SerializationKeys.createdAt]
    title <- map[SerializationKeys.title]
    pageId <- map[SerializationKeys.pageId]
  }


}
