//
//  MessageModel.swift
//
//  Created by Hamza Hasan on 22/06/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class MessageModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let status = "status"
    static let userFrom = "user_from"
    static let updatedAt = "updated_at"
    static let id = "id"
    static let sendFrom = "send_from"
    static let createdAt = "created_at"
    static let sendTo = "send_to"
    static let message = "message"
    static let subject = "subject"
  }

  // MARK: Properties
  @objc dynamic var status: String? = ""
  @objc dynamic var userFrom: String? = ""
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var id = 0
  @objc dynamic var sendFrom: String? = ""
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var sendTo: String? = ""
  @objc dynamic var message: String? = ""
  @objc dynamic var subject: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    status <- map[SerializationKeys.status]
    userFrom <- map[SerializationKeys.userFrom]
    updatedAt <- map[SerializationKeys.updatedAt]
    id <- map[SerializationKeys.id]
    sendFrom <- map[SerializationKeys.sendFrom]
    createdAt <- map[SerializationKeys.createdAt]
    sendTo <- map[SerializationKeys.sendTo]
    message <- map[SerializationKeys.message]
    subject <- map[SerializationKeys.subject]
  }


}
