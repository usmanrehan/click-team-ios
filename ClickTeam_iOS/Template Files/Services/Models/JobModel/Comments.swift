//
//  Comments.swift
//
//  Created by Hamza Hasan on 16/06/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class Comments: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updated_at"
    static let carriedOut = "carried_out"
    static let file = "file"
    static let quantity = "quantity"
    static let rating = "rating"
    static let status = "status"
    static let id = "id"
    static let taskId = "task_id"
    static let createdAt = "created_at"
    static let comment = "comment"
    static let userId = "user_id"
    static let images = "images"
  }

  // MARK: Properties
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var carriedOut: String? = ""
  var file = List<Images>()
  @objc dynamic var quantity: String? = ""
  @objc dynamic var rating: String? = ""
  @objc dynamic var status: String? = ""
  @objc dynamic var id: String? = ""
  @objc dynamic var taskId: String? = ""
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var comment: String? = ""
  @objc dynamic var userId: String? = ""
  var images = List<Images>()

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    updatedAt <- map[SerializationKeys.updatedAt]
    carriedOut <- map[SerializationKeys.carriedOut]
    file <- map[SerializationKeys.file]
    quantity <- map[SerializationKeys.quantity]
    rating <- map[SerializationKeys.rating]
    status <- map[SerializationKeys.status]
    id <- map[SerializationKeys.id]
    taskId <- map[SerializationKeys.taskId]
    createdAt <- map[SerializationKeys.createdAt]
    comment <- map[SerializationKeys.comment]
    userId <- map[SerializationKeys.userId]
    images <- (map[SerializationKeys.images], ListTransform<Images>())
  }


}
