//
//  JobModel.swift
//
//  Created by Hamza Hasan on 16/06/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class JobModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let completeDate = "complete_date"
    static let parentId = "parent_id"
    static let updatedAt = "updated_at"
    static let isAlreadyAssigned = "is_already_assigned"
    static let priority = "priority"
    static let status = "status"
    static let id = "id"
    static let createdBy = "created_by"
    static let parentTaskId = "parent_task_id"
    static let createdAt = "created_at"
    static let taskId = "task_id"
    static let comments = "comments"
    static let detail = "detail"
    static let title = "title"
    static let startDate = "start_date"
    static let endDate = "end_date"
    static let taskID = "task_id"
    static let ID = "id"
    static let cashValue = "cash_value"
    static let roles = "roles"
    static let earnPoint = "earn_point"
  }

  // MARK: Properties
  @objc dynamic var completeDate: String? = ""
  @objc dynamic var parentId: String? = ""
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var isAlreadyAssigned: String? = ""
  @objc dynamic var priority: String? = ""
  @objc dynamic var status: String? = ""
  @objc dynamic var id: String? = ""
  @objc dynamic var createdBy: String? = ""
  @objc dynamic var parentTaskId: String? = ""
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var taskId: String? = ""
  var comments = List<Comments>()
  @objc dynamic var detail: String? = ""
  @objc dynamic var title: String? = ""
  @objc dynamic var startDate: String? = ""
  @objc dynamic var endDate: String? = ""
  @objc dynamic var taskID = 0
  @objc dynamic var ID = 0
  @objc dynamic var cashValue: String? = ""
  @objc dynamic var roles: String? = ""
  @objc dynamic var earnPoint: String? = ""
  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    completeDate <- map[SerializationKeys.completeDate]
    parentId <- map[SerializationKeys.parentId]
    updatedAt <- map[SerializationKeys.updatedAt]
    isAlreadyAssigned <- map[SerializationKeys.isAlreadyAssigned]
    priority <- map[SerializationKeys.priority]
    status <- map[SerializationKeys.status]
    id <- map[SerializationKeys.id]
    createdBy <- map[SerializationKeys.createdBy]
    parentTaskId <- map[SerializationKeys.parentTaskId]
    createdAt <- map[SerializationKeys.createdAt]
    taskId <- map[SerializationKeys.taskId]
    comments <- (map[SerializationKeys.comments], ListTransform<Comments>())
    detail <- map[SerializationKeys.detail]
    title <- map[SerializationKeys.title]
    startDate <- map[SerializationKeys.startDate]
    endDate <- map[SerializationKeys.endDate]
    taskID <- map[SerializationKeys.taskID]
    ID <- map[SerializationKeys.ID]
    cashValue <- map[SerializationKeys.cashValue]
    roles <- map[SerializationKeys.roles]
    earnPoint <- map[SerializationKeys.earnPoint]
  }


}
