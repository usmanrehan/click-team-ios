//
//  ButtonStates.swift
//  Tobdon
//
//  Created by Usman Bin Rehan on 2/18/19.
//  Copyright © 2019 Usman Bin Rehan. All rights reserved.
//

import Foundation
import UIKit

class RedWhiteButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.borderWidth = 1
        self.borderColor = Global.APP_COLOR
        self.cornerRadius = 8
        self.clipsToBounds = true
        if self.isSelected{
            self.setTitleColor(.white, for: .selected)
            self.backgroundColor = Global.APP_COLOR
        }
        else{
            self.setTitleColor(Global.APP_COLOR, for: .normal)
            self.backgroundColor = .white
        }
    }
}
class BlueWhiteButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        if self.isSelected{
            self.setTitleColor(.white, for: .selected)
            self.backgroundColor = Global.APP_COLOR
        }
        else{
            self.setTitleColor(Global.APP_COLOR, for: .normal)
            self.backgroundColor = .white
        }
    }
}
class BlueWhiteBorderButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.borderWidth = 1.0
        self.borderColor = Global.APP_COLOR
        if self.isSelected{
            self.setTitleColor(.white, for: .selected)
            self.backgroundColor = Global.APP_COLOR
        }
        else{
            self.setTitleColor(Global.APP_COLOR, for: .normal)
            self.backgroundColor = .white
        }
    }
}
