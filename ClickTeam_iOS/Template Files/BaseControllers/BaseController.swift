//
//  BaseController.swift
//  The Court Lawyer
//
//  Created by Ahmed Shahid on 5/3/18.
//  Copyright © 2018 Ahmed Shahid. All rights reserved.
//

import Foundation
import UIKit

class BaseController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = Global.APP_COLOR
        // Do any additional setup after loading the view.
        guard let navControllerCount = self.navigationController?.viewControllers.count else {return}
        if navControllerCount > 1 {
            navigationController?.navigationBar.isHidden = false
            self.addBackBarButtonItem()
        }
        else{
            navigationController?.navigationBar.isHidden = true
        }
        let currentController = Utility.main.topViewController()?.className ?? ""
        if currentController == Maintenance().className{
            self.addPlusBarButtonItem()
        }
        if currentController == JobsList().className{
            self.addTaskPlusBarButtonItem()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        AppDelegate.shared.getCurrentWifiInfo()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    func addBackBarButtonItem() {
        let image = UIImage(named: "back")
        let backItem = UIBarButtonItem(image: image,
                                       style: .plain,
                                       target: self,
                                       action: #selector(onBtnBack))
        
        self.navigationItem.leftBarButtonItem = backItem
    }
    func addPlusBarButtonItem() {
        let image = UIImage(named: "plus")
        let editItem = UIBarButtonItem(image: image,
                                       style: .plain,
                                       target: self,
                                       action: #selector(onBtnAddMaintenance))
        self.navigationItem.rightBarButtonItem = editItem
    }
    func addTaskPlusBarButtonItem() {
        let image = UIImage(named: "plus")
        let editItem = UIBarButtonItem(image: image,
                                       style: .plain,
                                       target: self,
                                       action: #selector(onBtnAddTask))
        self.navigationItem.rightBarButtonItem = editItem
    }
    func addEditProfileBarButtonItem() {
        let image = UIImage(named: "edit")
        let editItem = UIBarButtonItem(image: image,
                                       style: .plain,
                                       target: self,
                                       action: #selector(onBtnEditProfile))
        
        self.navigationItem.rightBarButtonItem = editItem
    }
    func addNotificationBarButtonItem() {
        let image = UIImage(named: "notification")
        let editItem = UIBarButtonItem(image: image,
                                       style: .plain,
                                       target: self,
                                       action: #selector(onBtnNotifications))
        
        self.navigationItem.rightBarButtonItem = editItem
    }
    func addDoneBarButtonItem() {
    }
    func addTitle(title: String) {
        self.navigationController?.navigationBar.topItem?.title = title
    }
    @objc  func onBtnDone() {
        AppDelegate.shared.changeRootViewController()
    }
    @objc  func onBtnBack() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    @objc func onBtnAddMaintenance() {
        self.pushToJobDetail()
    }
    @objc func onBtnAddTask() {
        self.pushToAddTask()
    }
    @objc func onBtnEditProfile(){
        
    }
    @objc func onBtnNotifications() {
        //self.pushToNotifications()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
//MARK:- Helper Methods
extension BaseController{
//    private func pushToEditUserProfile(){
//        let storyBoard = AppStoryboard.User.instance
//        let controller = storyBoard.instantiateViewController(withIdentifier: "EditUserProfile")
//        self.navigationController?.pushViewController(controller, animated: true)
//    }
//    private func pushToNotifications(){
//        let storyBoard = AppStoryboard.User.instance
//        let controller = storyBoard.instantiateViewController(withIdentifier: "Notifications")
//        self.navigationController?.pushViewController(controller, animated: true)
//    }
    private func pushToJobDetail(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "JobDetail") as! JobDetail
        controller.jobDetailType = .addMaintenance
        controller.carriedOutType = .CarriedOutYes
        self.navigationController?.pushViewController(controller, animated: true)
    }
    private func pushToAddTask(){
        let storyboard = AppStoryboard.Home_a.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "AddTask") as! AddTask
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
extension UIViewController {
    var className: String {
        return NSStringFromClass(self.classForCoder).components(separatedBy: ".").last!;
    }
}
