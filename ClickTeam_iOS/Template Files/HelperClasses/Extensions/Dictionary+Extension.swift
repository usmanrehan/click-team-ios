//
//  Dictionary+Extension.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 07/06/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import Foundation

extension Dictionary {
    var json: String {
        let invalidJson = "Not a valid JSON"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? invalidJson
        } catch {
            return invalidJson
        }
    }
    func printJson() {
        print(json)
    }
}
