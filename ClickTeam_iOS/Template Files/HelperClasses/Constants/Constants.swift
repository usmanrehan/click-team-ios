
import Foundation
import UIKit

struct Global{
    static let LOGGED_IN_USER                = AppStateManager.sharedInstance.loggedInUser
    static var APP_MANAGER                   = AppStateManager.sharedInstance
    static var APP_REALM                     = APP_MANAGER.realm
    static var APP_COLOR                 = UIColor(red:25/255, green:73/255, blue:153/255, alpha:1.0)
}

struct Constants {
    //MARK:- Base URL
    static let BaseURL                     = "http://clickteam.asia/api/v1/"
    static let APP_DELEGATE                = UIApplication.shared.delegate as! AppDelegate
    static let UIWINDOW                    = UIApplication.shared.delegate!.window!
    static let USER_DEFAULTS               = UserDefaults.standard
    static let SINGLETON                   = Singleton.sharedInstance
    static let DEFAULTS_USER_KEY           = "User"
    static var DeviceToken                 = "No certificates"
    static let serverDateFormat            = "yyyy-MM-dd HH:mm:ss"
    static let PAGINATION_PAGE_SIZE        = 100
    static var AuthToken                   = ""
    //MARK:- Notification observer names
    static let NotificationCount           = "NotificationCount"
    static var ConnectedWifi               = ""
}
