//
//  Strings.swift
//  Gym Buzz
//
//  Created by Usman Bin Rehan on 8/21/18.
//  Copyright © 2018 Usman Bin Rehan. All rights reserved.
//

import Foundation

enum Strings: String{
    //MARK:- Notifies
    case ALERT = "Alert"
    case ERROR = "Error"
    case UNKNOWN_ERROR = "Unknown error"
    case YES = "Yes"
    case NO = "No"
    case OK = "Ok"
    case SUCCESS = "Success"
    case Confirmation = "Confirmation"
    case PIN_SENT_EMAIL = "A verification PIN has been sent to your provided email address."
    
    //MARK:- Validation
    case EMPTY_LOGIN_FIELDS = "Please provide your valid email address or mobile number to login."
    case INVALID_NAME = "Name should contain atleast 3 characters."
    case INVALID_EMAIL = "Please provide a valid Email Address."
    case INVALID_PHONE = "Please provide a valid Phone Number."
    case INVALID_COUNTRY = "Please select your country."
    case EMPTY_PWD = "Please provide password."
    case EMPTY_CONFIRM_PWD = "Please confirm your password."
    case INVALID_PWD = "Password should contain atleast 6 characters."
    case PWD_ATLEAST_SIX_CH = "At least 6 characters."
    case PWD_DONT_MATCH = "New password and confirm password does not match."
    case ALL_FIELD_REQ = "All Fields are required!"
    case INVALID_PIN = "Please provide complete PIN."
    case PWD_CHANGED = "You have successfully updated your password."
    case URL_NOT_VALID = "Invalid URL"
    case LOGOUT = "Logout"
    case ASK_LOGOUT = "Are you sure you want to logout?"
    
    case ERROR_GENERIC_MESSAGE = "Unable to connect server\n Please check your internet connection and try again later."
    case TOKEN_EXPIRED = "Invalid Authentication Token Supplied."
    case DELETED_USER = "Your account has been deleted by admin."
    case JOB_DONE = "Job Done"
    case ASK_FOR_COMPLETE_JOB = "Are you sure you want to\ncomplete this job?"
    case ASSIGN_JOB = "Assign Job"
    case ASK_FOR_ASSIGN_JOB = "Are you sure you want to\nassign this job to "
    case CANCEL_ASSIGN_JOB = "Cancel assigned Job"
    case ASK_FOR_RESIGN_JOB = "Are you sure you want to\ncancel assigned job of "
    case ACKNOWLEDGE_JOB = "Acknowledge Job"
    case ASK_FOR_ACKNOWLEDGE_JOB = "Are you sure you want to\nacknowledge this job?"
    //MARK:- Error messages
    case RESPONSE_ERROR = "Invalid response for route:"
    
    //SingleMethod For the Usage of Localization.
    var text: String { return NSLocalizedString( self.rawValue, comment: "") }
}
