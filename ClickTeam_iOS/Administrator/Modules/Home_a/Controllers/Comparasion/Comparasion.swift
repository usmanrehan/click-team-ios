//
//  Comparasion.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 14/06/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import ObjectMapper

class Comparasion: BaseController {

    var arrEmployees = [ComparasionModel]()
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Comparison"
        self.registerCell()
        self.getComparasion()
        // Do any additional setup after loading the view.
    }
}
//MARK:- Helper Methods
extension Comparasion{
    private func getDate()->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: Date())
    }
    private func registerCell(){
        self.tableView.register(UINib(nibName: "ComparasionTVC", bundle: nil), forCellReuseIdentifier: "ComparasionTVC")
    }
}
extension Comparasion:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrEmployees.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ComparasionTVC", for: indexPath) as! ComparasionTVC
        cell.setData(data: self.arrEmployees[indexPath.row])
        return cell
    }
}
//MARK:- Services
extension Comparasion{
    private func getComparasion(){
        let date = self.getDate()
        let params:[String:Any] = ["date":date]
        APIManager.sharedInstance.usersAPIManager.GetComparasion(params: params, success: { (responseObject) in
            print(responseObject)
            guard let employees = responseObject as? [[String : Any]] else {return}
            self.arrEmployees = Mapper<ComparasionModel>().mapArray(JSONArray: employees)
            self.tableView.reloadData()
            if self.arrEmployees.isEmpty{
                Utility.main.showToast(message: "No records available")
            }
        }) { (error) in
            print(error)
        }
    }
}
