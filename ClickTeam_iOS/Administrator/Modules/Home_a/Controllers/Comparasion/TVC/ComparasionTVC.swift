//
//  ComparasionTVC.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 14/06/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import Cosmos

class ComparasionTVC: UITableViewCell {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblEmployeeOfTheMonth: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDesignation: UILabel!
    @IBOutlet weak var lblPerformance: UILabel!
    @IBOutlet weak var lblAttendence: UILabel!
    @IBOutlet weak var lblConformance: UILabel!
    @IBOutlet weak var viewRating: CosmosView!
    
    func setData(data:ComparasionModel){
        if let profileImage = data.details?.image{
            if let profileImageURL = URL(string: profileImage){
                self.imgProfile.sd_setImage(with: profileImageURL, completed: nil)
            }
        }
        if data.eom{
            self.lblEmployeeOfTheMonth.text = "Employee Of The Month"
        }
        else{
            self.lblEmployeeOfTheMonth.text = ""
        }
        self.lblName.text = data.name ?? "-"
        self.lblDesignation.text = data.roles ?? "-"
        self.lblPerformance.text = "\(data.performancePoints)"
        self.lblAttendence.text = "\(data.attendance)"
        self.lblConformance.text = "\(data.conformancePoints)"
        self.viewRating.rating = Double(data.ratings)
    }
    
}
