//
//  EarningsView.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 10/06/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import DropDown
import ObjectMapper

enum EarningType{
    case none
    case performance
    case conformance
    case attendance
    case deduction
}

class Earnings_a: BaseController {

    @IBOutlet weak var btnSelectRole: UIButton!
    @IBOutlet weak var btnPerformance: BlueWhiteButton!
    @IBOutlet weak var btnConformance: BlueWhiteButton!
    @IBOutlet weak var btnAttendance: BlueWhiteButton!
    @IBOutlet weak var btnDeduction: BlueWhiteButton!
    @IBOutlet weak var tableView: UITableView!
    
    let dropDown = DropDown()
    var arrRoleModel = [RoleModel]()
    var selectedRole: RoleModel?
    var earningType = EarningType.none
    var deduction: DeductionModel?
    var arrEmployees = [EmployeeRecord]()
    var selectedEmployee: EmployeeRecord?
    var arrPerformance = [TaskModel]()
    var arrConformance = [TaskModel]()
    var selectedPerformance = TaskModel()
    var selectedConformance = TaskModel()
    var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Earnings"
        self.registerCell()
        self.getRoles()
        self.getAllEmployees()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnSelectRole(_ sender: UIButton) {
        if self.arrRoleModel.isEmpty{
            self.getRoles()
        }
        if self.arrEmployees.isEmpty{
            self.getAllEmployees()
        }
        self.dropDown.show()
    }
    @IBAction func onBtnEarningType(_ sender: BlueWhiteButton) {
        if sender.isSelected{return}
        switch sender.tag {
        case 0:
            self.btnPerformance.isSelected = true
            self.btnConformance.isSelected = false
            self.btnAttendance.isSelected = false
            self.btnDeduction.isSelected = false
            self.earningType = .performance
            self.setDropDownRoles()
        case 1:
            self.btnPerformance.isSelected = false
            self.btnConformance.isSelected = true
            self.btnAttendance.isSelected = false
            self.btnDeduction.isSelected = false
            self.earningType = .conformance
            self.setDropDownRoles()
        case 2:
            self.btnPerformance.isSelected = false
            self.btnConformance.isSelected = false
            self.btnAttendance.isSelected = true
            self.btnDeduction.isSelected = false
            self.earningType = .attendance
            self.setDropDownRoles()
        case 3:
            self.btnPerformance.isSelected = false
            self.btnConformance.isSelected = false
            self.btnAttendance.isSelected = false
            self.btnDeduction.isSelected = true
            self.earningType = .deduction
            self.setDropDownEmployees()
        default:
            break
        }
        if sender.tag == 3{ //Deduction
            self.checkForEmployee()
        }
        else{
            self.checkForRole()
        }
        self.tableView.reloadData()
    }
}
//MARK:- Helper Methods
extension Earnings_a{
    private func setDropDownRoles(){
        self.resetEmployee()
        self.resetRole()
        self.dropDown.anchorView = self.btnSelectRole
        self.dropDown.direction = .bottom
        var roles = [String]()
        for item in self.arrRoleModel{
            roles.append(item.displayName ?? "-")
        }
        self.dropDown.dataSource = roles
        self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            let data = self.arrRoleModel[index]
            self.setRole(data: data)
        }
    }
    private func setRole(data:RoleModel){
        self.selectedRole = data
        let displayName = data.displayName ?? "-"
        self.btnSelectRole.setTitle(displayName, for: .normal)
        if self.earningType == .performance{
            self.getPerformance()
        }
        if self.earningType == .conformance{
            self.getConformance()
        }
        if self.earningType == .attendance{
            self.tableView.reloadData()
        }
    }
    private func resetRole(){
        self.selectedRole = nil
        self.btnSelectRole.setTitle("Select Role", for: .normal)
        if self.earningType == .performance{
            self.arrPerformance.removeAll()
            self.tableView.reloadData()
        }
        if self.earningType == .conformance{
            self.arrConformance.removeAll()
            self.tableView.reloadData()
        }
    }
    private func setDropDownEmployees(){
        self.resetRole()
        self.resetEmployee()
        self.dropDown.anchorView = self.btnSelectRole
        self.dropDown.direction = .bottom
        var roles = [String]()
        for item in self.arrEmployees{
            roles.append(item.name ?? "-")
        }
        self.dropDown.dataSource = roles
        self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            let data = self.arrEmployees[index]
            self.setEmployee(data: data)
        }
    }
    private func setEmployee(data:EmployeeRecord){
        self.selectedEmployee = data
        let displayName = data.name ?? "-"
        self.btnSelectRole.setTitle(displayName, for: .normal)
    }
    private func resetEmployee(){
        self.selectedEmployee = nil
        self.btnSelectRole.setTitle("Select Employee", for: .normal)
    }
    private func registerCell(){
        self.tableView.register(UINib(nibName: "UpdatePerformanceConformanceTVC", bundle: nil), forCellReuseIdentifier: "UpdatePerformanceConformanceTVC")
        self.tableView.register(UINib(nibName: "AttendanceTVC", bundle: nil), forCellReuseIdentifier: "AttendanceTVC")
        self.tableView.register(UINib(nibName: "DeductionTVC", bundle: nil), forCellReuseIdentifier: "DeductionTVC")
    }
    private func checkForRole(){
        if self.selectedRole == nil{
            Utility.main.showToast(message: "Please select a role.")
            return
        }
    }
    private func checkForEmployee(){
        if self.selectedEmployee == nil{
            Utility.main.showToast(message: "Please select an employee.")
            return
        }
    }
}
//MARK:- Selector Methods
extension Earnings_a{
    @objc func onBtnSavePerformance(sender:UIButton){
        self.selectedIndex = sender.tag
        self.selectedPerformance = self.arrPerformance[sender.tag]
        Utility.main.showAlert(message: "Are you sure you want to process this update?", title: "Update Performance", controller: self) { (yes, no) in
            if yes != nil{
                self.updatePerformance()
            }
        }
    }
    @objc func onBtnSaveConformance(sender:UIButton){
        self.selectedIndex = sender.tag
        self.selectedConformance = self.arrConformance[sender.tag]
        Utility.main.showAlert(message: "Are you sure you want to process this update?", title: "Update Conformance", controller: self) { (yes, no) in
            if yes != nil{
                self.updateConformance()
            }
        }
    }
    @objc func onBtnSaveAttendance(){
        if self.selectedRole == nil{
            Utility.main.showToast(message: "Please select a role.")
            return
        }
        self.setWorkingHours()
    }
    @objc func onBtnSubmitDeduction(){
        self.checkForEmployee()
        if self.selectedEmployee != nil{
            Utility.main.showAlert(message: "Are you sure you want to process this deduction?", title: "Confirmation", controller: self) { (yes, no) in
                if yes != nil{
                    self.setDeductions()
                }
            }
        }
    }
}
//MARK:- UITableViewDataSource
extension Earnings_a:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.earningType{
        case .none:
            return 0
        case .performance:
            return self.arrPerformance.count
        case .conformance:
            return self.arrConformance.count
        case .attendance:
            return 1
        case .deduction:
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch self.earningType{
        case .none:
            return UITableViewCell()
        case .performance:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpdatePerformanceConformanceTVC", for: indexPath) as! UpdatePerformanceConformanceTVC
            cell.btnSave.tag = indexPath.row
            cell.btnSave.addTarget(self, action: #selector(self.onBtnSavePerformance(sender:)), for: .touchUpInside)
            let data = self.arrPerformance[indexPath.row]
            cell.setData(data: data)
            return cell
        case .conformance:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpdatePerformanceConformanceTVC", for: indexPath) as! UpdatePerformanceConformanceTVC
            cell.btnSave.tag = indexPath.row
            cell.btnSave.addTarget(self, action: #selector(self.onBtnSaveConformance(sender:)), for: .touchUpInside)
            let data = self.arrConformance[indexPath.row]
            cell.setData(data: data)
            return cell
        case .attendance:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AttendanceTVC", for: indexPath) as! AttendanceTVC
            cell.resetData(data: self.selectedRole)
            cell.btnSave.addTarget(self, action: #selector(self.onBtnSaveAttendance), for: .touchUpInside)
            return cell
        case .deduction:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DeductionTVC", for: indexPath) as! DeductionTVC
            cell.resetData()
            cell.btnSubmit.addTarget(self, action: #selector(self.onBtnSubmitDeduction), for: .touchUpInside)
            return cell
        }
    }
}
//MARK:- Services
extension Earnings_a{
    private func getRoles(){
        APIManager.sharedInstance.usersAPIManager.GetRoles(params: [:], success: { (responseObject) in
            print(responseObject)
            guard let roles = responseObject as? [[String : Any]] else {return}
            self.arrRoleModel = Mapper<RoleModel>().mapArray(JSONArray: roles)
            self.setDropDownRoles()
        }) { (error) in
            print(error)
        }
    }
    private func setDeductions(){
        let user_id = self.selectedEmployee?.id ?? 0
        var params:[String:Any] = ["role_id":user_id,]
        let deductionCellIndex = IndexPath(row: 0, section: 0)
        if let cell = self.tableView.cellForRow(at: deductionCellIndex) as? DeductionTVC{
            let incometax = cell.tfIncomeTax.text ?? "0"
            let loss_caused = cell.tfLossCausedToCompany.text ?? "0"
            let ssnit = cell.tfSsnit.text ?? "0"
            params["incometax"] = incometax
            params["loss_caused"] = loss_caused
            params["ssnit"] = ssnit
        }
        print(params)
        APIManager.sharedInstance.usersAPIManager.SetDeductions(params: params, success: { (responseObject) in
            self.tableView.reloadData()
        }) { (error) in
            print(error)
        }
    }
    private func getAllEmployees(){
        APIManager.sharedInstance.usersAPIManager.GetAllEmployees(params: [:], success: { (responseObject) in
            print(responseObject)
            guard let employees = responseObject as? [[String : Any]] else {return}
            self.arrEmployees = Mapper<EmployeeRecord>().mapArray(JSONArray: employees)
            if self.arrEmployees.isEmpty{
                Utility.main.showToast(message: "No employee available")
            }
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    private func getPerformance(){
        let role = self.selectedRole?.id ?? 0
        let type = "1"
        let params:[String:Any] = ["role":role,"type":type]
        APIManager.sharedInstance.usersAPIManager.GetAllJobs(params: params, success: { (responseObject) in
            print(responseObject)
            guard let performanceTasks = responseObject as? [[String : Any]] else {return}
            self.arrPerformance = Mapper<TaskModel>().mapArray(JSONArray: performanceTasks)
            self.tableView.reloadData()
            if self.arrPerformance.isEmpty{
                Utility.main.showToast(message: "No record available.")
            }
        }) { (error) in
            print(error)
        }
    }
    private func getConformance(){
        let role = self.selectedRole?.id ?? 0
        let type = "2"
        let params:[String:Any] = ["role":role,"type":type]
        APIManager.sharedInstance.usersAPIManager.GetAllJobs(params: params, success: { (responseObject) in
            print(responseObject)
            guard let conformanceTasks = responseObject as? [[String : Any]] else {return}
            self.arrConformance = Mapper<TaskModel>().mapArray(JSONArray: conformanceTasks)
            self.tableView.reloadData()
            if self.arrConformance.isEmpty{
                Utility.main.showToast(message: "No record available.")
            }
        }) { (error) in
            print(error)
        }
    }
    private func updatePerformance(){
        let id = self.selectedPerformance.id ?? "0"
        var params: [String:Any] = [:]
        let index = IndexPath(row: self.selectedIndex, section: 0)
        if let cell = tableView.cellForRow(at: index) as? UpdatePerformanceConformanceTVC{
            let title = cell.tfJobTitle.text ?? ""
            let earn_point = cell.tfPointValue.text ?? ""
            let cash_value = cell.tfCashValue.text ?? ""
            params["title"] = title
            params["earn_point"] = earn_point
            params["cash_value"] = cash_value
        }
        print(params)
        APIManager.sharedInstance.usersAPIManager.UpdatePerformance(params: params, success: { (responseObject) in
            self.view.endEditing(true)
        }, failure: { (error) in
            print(error)
        }, id: id)
    }
    private func updateConformance(){
        let id = self.selectedConformance.id ?? "0"
        var params: [String:Any] = [:]
        let index = IndexPath(row: self.selectedIndex, section: 0)
        if let cell = tableView.cellForRow(at: index) as? UpdatePerformanceConformanceTVC{
            let title = cell.tfJobTitle.text ?? ""
            let earn_point = cell.tfPointValue.text ?? ""
            let cash_value = cell.tfCashValue.text ?? ""
            params["title"] = title
            params["earn_point"] = earn_point
            params["cash_value"] = cash_value
        }
        APIManager.sharedInstance.usersAPIManager.UpdatePerformance(params: params, success: { (responseObject) in
            self.view.endEditing(true)
        }, failure: { (error) in
            print(error)
        }, id: id)
    }
    private func setWorkingHours(){
        let id = self.selectedRole?.id ?? 0
        let name = self.selectedRole?.name ?? ""
        let display_name = self.selectedRole?.displayName ?? ""
        var params: [String:Any] = ["id":id,
                                    "name":name,
                                    "display_name":display_name]
        let index = IndexPath(row: 0, section: 0)
        if let cell = tableView.cellForRow(at: index) as? AttendanceTVC{
            let Working_hours = cell.tfWorkingHours.text ?? ""
            params["Working_hours"] = Working_hours
        }
        print(params)
        APIManager.sharedInstance.usersAPIManager.updateRole(params: params, success: { (responseObject) in
            self.resetRole()
            self.tableView.reloadData()
            self.view.endEditing(true)
            self.getRoles()
        }) { (error) in
            print(error)
        }
    }
}
