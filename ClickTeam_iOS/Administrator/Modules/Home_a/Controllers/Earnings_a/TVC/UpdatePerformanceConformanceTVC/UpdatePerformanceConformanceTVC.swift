//
//  UpdatePerformanceConformanceTVC.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 15/06/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit

class UpdatePerformanceConformanceTVC: UITableViewCell {

    @IBOutlet weak var tfJobTitle: UITextField!
    @IBOutlet weak var tfPointValue: UITextField!
    @IBOutlet weak var tfCashValue: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    
    func setData(data:TaskModel){
        self.tfJobTitle.text = data.title ?? "-"
        self.tfPointValue.text = data.earnPoint ?? "0"
        self.tfCashValue.text = data.cashValue ?? "0"
    }
}

