//
//  DeductionTVC.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 15/06/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit

class DeductionTVC: UITableViewCell {

    @IBOutlet weak var tfIncomeTax: UITextField!
    @IBOutlet weak var tfLossCausedToCompany: UITextField!
    @IBOutlet weak var tfSsnit: UITextField!
    @IBOutlet weak var tfOther: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    
    func resetData(){
        self.tfIncomeTax.text = ""
        self.tfLossCausedToCompany.text = ""
        self.tfSsnit.text = ""
        self.tfOther.text = ""
    }
}
