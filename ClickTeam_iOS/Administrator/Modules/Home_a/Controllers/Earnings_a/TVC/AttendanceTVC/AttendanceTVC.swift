//
//  AttendanceTVC.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 15/06/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit

class AttendanceTVC: UITableViewCell {
    @IBOutlet weak var tfWorkingHours: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    
    func resetData(data:RoleModel?){
        self.tfWorkingHours.text = data?.workingHours
    }
}
