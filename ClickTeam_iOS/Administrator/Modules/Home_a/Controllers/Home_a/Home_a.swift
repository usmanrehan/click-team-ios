//
//  Home_a.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 19/05/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit

class Home_a: UIViewController {

    @IBOutlet weak var imgProfile: RoundedImage!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setData()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnLogout(_ sender: UIButton) {
        AppStateManager.sharedInstance.logoutUser()
    }
    @IBAction func onBtnNotification(_ sender: UIButton) {
        self.pushToNotifications()
    }
    @IBAction func onBtnAssignDuties(_ sender: UIButton) {
        self.pushToUsersList(type: .assignDuties)
    }
    @IBAction func onBtnDailyAssessments(_ sender: UIButton) {
        self.pushToUsersList(type: .dailyAssessments)
    }
    @IBAction func onBtnEarnings(_ sender: UIButton) {
        self.pushToEarnings()
    }
    @IBAction func onBtnMaintenanceRequests(_ sender: UIButton) {
        self.pushToMaintenanceAssessments()
    }
    @IBAction func onBtnReminder(_ sender: UIButton) {
        self.pushToUsersList(type: .reminders)
    }
    @IBAction func onBtnEmployeeReport(_ sender: UIButton) {
        self.pushToEmployeeRecord()
    }
    @IBAction func onBtnMessages(_ sender: UIButton) {
        self.pushToUsersList(type: .sendMessage)
    }
    @IBAction func onBtnCompany(_ sender: UIButton) {
        self.pushToCompany()
    }
    @IBAction func onBtnUsers(_ sender: UIButton) {
        self.pushToUsersList(type: .editUserDetail)
    }
}
extension Home_a{
    private func pushToNotifications(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Notifications")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    private func pushToEarnings(){
        let storyboard = AppStoryboard.Home_a.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Earnings_a")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    private func pushToUsersList(type:CategoryType){
        let storyboard = AppStoryboard.Home_a.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "UsersList") as! UsersList
        controller.categoryType = type
        self.navigationController?.pushViewController(controller, animated: true)
    }
    private func pushToCompany(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Company")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    private func pushToMaintenanceAssessments(){
        let storyboard = AppStoryboard.Home_a.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "MaintenanceAssessment") as! MaintenanceAssessment
        self.navigationController?.pushViewController(controller, animated: true)
    }
    private func pushToEmployeeRecord(){
        let storyboard = AppStoryboard.Home_a.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "EmployeeRecordVC")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    private func setData(){
        let user = AppStateManager.sharedInstance.loggedInUser
        self.lblUserName.text = user?.name ?? "-"
        self.lblEmail.text = user?.email ?? "-"
    }
}
