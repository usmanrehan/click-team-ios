//
//  ReminderTVC.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 17/06/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit

class ReminderTVC: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    func setData(data:ReminderModel){
        self.lblTitle.text = data.reminderTitle ?? "-"
        self.lblDescription.text = data.reminderDescription ?? "-"
        self.lblDate.text = data.reminderDate ?? "-"
    }
}
