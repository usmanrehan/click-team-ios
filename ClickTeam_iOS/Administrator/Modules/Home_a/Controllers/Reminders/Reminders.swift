//
//  Reminders.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 17/06/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import ObjectMapper

class Reminders: BaseController {
    
    @IBOutlet weak var tableView: UITableView!
    var arrReminders = [ReminderModel]()
    var employee = EmployeeRecord()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Reminders"
        self.registerCell()
        self.getReminders()
        self.addPlusBarButtonItem()
        // Do any additional setup after loading the view.
    }
    override func onBtnAddMaintenance() {
        self.presentSubmitReminder()
    }
}
//MARK:- Helper Methods
extension Reminders{
    private func registerCell(){
        self.tableView.register(UINib(nibName: "ReminderTVC", bundle: nil), forCellReuseIdentifier: "ReminderTVC")
    }
    private func presentSubmitReminder(){
        let storyboard = AppStoryboard.Home_a.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "AddReminder") as! AddReminder
        controller.reminderSubmitted = { reminder in
            self.submitReminder(reminder: reminder)
        }
        self.present(controller, animated: true, completion: nil)
    }
}
//MARK:- UITableViewDataSource
extension Reminders:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrReminders.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReminderTVC", for: indexPath) as! ReminderTVC
        cell.setData(data: self.arrReminders[indexPath.row])
        return cell
    }
}
//MARK:- Services
extension Reminders{
    private func getReminders(){
        self.arrReminders.removeAll()
        self.tableView.reloadData()
        let user_id = self.employee.id
        let search = "user_id:\(user_id)"
        let params:[String:Any] = ["search":search]
        APIManager.sharedInstance.usersAPIManager.GetReminders(params: params, success: { (responseObject) in
            print(responseObject)
            guard let reminders = responseObject as? [[String : Any]] else {return}
            self.arrReminders = Mapper<ReminderModel>().mapArray(JSONArray: reminders)
            self.tableView.reloadData()
            if self.arrReminders.isEmpty{
                Utility.main.showToast(message: "No reminders available")
            }
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    private func submitReminder(reminder:ReminderData){
        let user_id = self.employee.id
        let reminder_title = reminder.title
        let reminder_description = reminder.description
        let reminder_date = reminder.date
        let params:[String:Any] = ["user_id":user_id,
                                   "reminder_title":reminder_title,
                                   "reminder_description":reminder_description,
                                   "reminder_date":reminder_date]
        
        APIManager.sharedInstance.usersAPIManager.SetReminder(params: params, success: { (responseObject) in
            print(responseObject)
            Utility.main.showToast(message: "Reminder added successfully")
            self.getReminders()
        }) { (error) in
            print(error)
        }
    }
}
