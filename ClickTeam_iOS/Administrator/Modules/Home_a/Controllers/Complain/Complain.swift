//
//  Complain.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 16/06/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import Cosmos

class Complain: BaseController {
    
    @IBOutlet weak var tfTitle: UITextField!
    @IBOutlet weak var tfDate: UITextField!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tvComment: UITextView!
    
    var employee = EmployeeRecord()
    var imagePicker: UIImagePickerController!
    var arrImages = [UIImage]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Complain"
        self.setDate(date: Date())
        self.registerCVC()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onTfDatePicker(_ sender: UITextField) {
        self.getPickedDate(sender)
    }
    @IBAction func btnUploadPhoto(_ sender: UIButton) {
        self.uploadImage()
    }
    @IBAction func onBtnSubmit(_ sender: UIButton) {
        Utility.main.showAlert(message: "Are you sure you want to process this complain?", title: "Confirmation", controller: self) { (yes, no) in
            if yes != nil{
                self.submitComplain()
            }
        }
    }

}
extension Complain{
    private func registerCVC(){
        self.collectionView.register(UINib(nibName: "ImageCVC", bundle: nil), forCellWithReuseIdentifier: "ImageCVC")
    }
}
//MARK:- Helper Methods
extension Complain{
    private func setDate(date:Date){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd, MMM yyyy"
        self.tfDate.text = dateFormatter.string(from: date)
    }
    private func params()->[String:Any]{
        let user_id = self.employee.id
        let complain = self.tfTitle.text ?? ""
        let complain_rate = Int(self.ratingView.rating)
        let complain_comment = self.tvComment.text ?? ""
        var params:[String:Any] = ["user_id":user_id,
                                   "complain":complain,
                                   "complain_rate":complain_rate,
                                   "complain_comment":complain_comment]
        for (index,image) in self.arrImages.enumerated(){
            let imageData = image.jpegData(compressionQuality: 0.1) ?? Data()
            params["images[\(index)]"] = imageData
        }
        return params
    }
    private func showSuccessAlert(){
        Utility.main.showAlert(message: "Complain registered successfully.", title: "Complain", controller: self) {
            self.navigationController?.popViewController(animated: true)
        }
    }
}
//MARK:- Date picker
extension Complain{
    private func getPickedDate(_ sender: UITextField){
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = .date
        datePickerView.maximumDate = Date()
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: .valueChanged)
    }
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        self.setDate(date: sender.date)
    }
}
extension Complain:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrImages.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCVC", for: indexPath) as! ImageCVC
        cell.setData(image: self.arrImages[indexPath.item])
        return cell
    }
}
extension Complain:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let length = collectionView.frame.height
        let size = CGSize(width: length, height: length)
        return size
    }
}
//MARK:- Image picker
extension Complain{
    func uploadFromCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            self.imagePicker =  UIImagePickerController()
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .camera
            Utility.main.topViewController()?.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    func uploadFromGallery(){
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        Utility.main.topViewController()?.present(picker, animated: true, completion: nil)
    }
    func uploadImage(){
        let alert = UIAlertController(title: "Upload Photo" , message: "How do you want to set your photo?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (UIAlertAction) in
            self.uploadFromCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (UIAlertAction) in
            self.uploadFromGallery()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        Utility.main.topViewController()?.present(alert, animated: true, completion: nil)
    }
}
//MARK:- UIImagePickerControllerDelegate
extension Complain: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            self.arrImages.append(pickedImage)
            self.collectionView.reloadData()
        }
        Utility.main.topViewController()?.dismiss(animated: true, completion: nil)
    }
}
//MARK:- Service
extension Complain{
    private func submitComplain(){
        APIManager.sharedInstance.usersAPIManager.AddComplain(params: self.params(), success: { (responseObject) in
            self.showSuccessAlert()
        }) { (error) in
            print(error)
        }
    }
}
