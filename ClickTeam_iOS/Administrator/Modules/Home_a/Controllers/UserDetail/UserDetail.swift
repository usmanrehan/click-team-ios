//
//  UserDetail.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 16/06/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import ObjectMapper
import DropDown

class UserDetail: BaseController {

    @IBOutlet weak var tfUserId: UITextField!
    @IBOutlet weak var tfFullName: UITextField!
    @IBOutlet weak var tfEmailAddress: UITextField!
    @IBOutlet weak var tfPhoneNumber: UITextField!
    @IBOutlet weak var tfUserRole: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    
    var employee = EmployeeRecord()
    var arrRoleModel = [Roles]()
    var selectedRole: Roles?
    let dropDown = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "User Detail"
        self.getRoles()
        self.setData()
        // Do any additional setup after loading the view.
    }
   
    @IBAction func onBtnUpdate(_ sender: UIButton) {
        self.validate(sender: sender)
    }
    @IBAction func onBtnComplain(_ sender: UIButton) {
        self.pushToComplain()
    }
}
//MARK:- Helper Methods
extension UserDetail{
    private func setData(){
        self.tfUserId.text = "\(self.employee.id)"
        self.tfFullName.text = self.employee.name
        self.tfEmailAddress.text = self.employee.email
        self.tfPhoneNumber.text = self.employee.details?.phone
        self.tfUserRole.text = self.employee.roles.first?.displayName
        self.selectedRole = self.employee.roles.first
    }
    private func setDropDownRoles(){
        self.dropDown.anchorView = self.tfUserRole
        self.dropDown.direction = .top
        var roles = [String]()
        for item in self.arrRoleModel{
            roles.append(item.displayName ?? "-")
        }
        self.dropDown.dataSource = roles
        self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            let data = self.arrRoleModel[index]
            self.setRole(data: data)
        }
    }
    private func setRole(data:Roles){
        self.selectedRole = data
        let displayName = data.displayName ?? "-"
        self.tfUserRole.text = displayName
    }
    private func validate(sender:UIButton){
        let name = self.tfFullName.text ?? ""
        let email = self.tfEmailAddress.text ?? ""
        let phone = self.tfPhoneNumber.text ?? ""
        let role = self.tfUserRole.text ?? ""
        
        if !Validation.isValidName(name){
            Utility.main.showToast(message: Strings.INVALID_NAME.text)
            sender.shake()
            return
        }
        if !Validation.isValidEmail(email){
            Utility.main.showToast(message: Strings.INVALID_EMAIL.text)
            sender.shake()
            return
        }
        if !Validation.isValidPhoneNumber(phone){
            Utility.main.showToast(message: Strings.INVALID_PHONE.text)
            sender.shake()
            return
        }
        if !Validation.validateStringLength(role){
            Utility.main.showToast(message: "Please select a role.")
            sender.shake()
            return
        }
        Utility.main.showAlert(message: "Are you sure you want to update this user's record?", title: "Confirmation", controller: self) { (yes, no) in
            if yes != nil{
                self.updateUser()
            }
        }
        
    }
    private func pushToComplain(){
        let storyboard = AppStoryboard.Home_a.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Complain") as! Complain
        controller.employee = self.employee
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
//MARK:- Helper Methods
extension UserDetail:UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.dropDown.show()
        return false
    }
}
//MARK:- Services
extension UserDetail{
    private func getRoles(){
        APIManager.sharedInstance.usersAPIManager.GetRoles(params: [:], success: { (responseObject) in
            print(responseObject)
            guard let roles = responseObject as? [[String : Any]] else {return}
            self.arrRoleModel = Mapper<Roles>().mapArray(JSONArray: roles)
            self.setDropDownRoles()
        }) { (error) in
            print(error)
        }
    }
    private func updateUser(){
        let id = "\(self.employee.id)"
        let first_name = self.tfFullName.text ?? ""
        let phone = self.tfPhoneNumber.text ?? ""
        let role_id = self.selectedRole?.id ?? 0
        let password = self.tfPassword.text ?? ""
        var params:[String:Any] = ["name":first_name,
                                  "phone":phone,
                                  "roles[]":role_id]
        if !password.isEmpty{
            params["password"] = password
            params["password_confirmation"] = password
        }
        APIManager.sharedInstance.usersAPIManager.UpdateUser(params: params, success: { (responseObject) in
            print(responseObject)
            Utility.main.showAlert(message: "Record updated successfully", title: "Success", controller: self, usingCompletionHandler: {
                self.navigationController?.popViewController(animated: true)
            })
        }, failure: { (error) in
            print(error)
        }, id: id)
    }
}
