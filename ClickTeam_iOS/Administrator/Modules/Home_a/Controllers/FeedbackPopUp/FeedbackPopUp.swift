//
//  FeedbackPopUp.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 22/05/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import Cosmos

struct FeedbackData {
    var rating:String
    var comment:String
}

class FeedbackPopUp: UIViewController {

    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var tvComment: UITextView!
    
    var feedbackSubmitted: ((FeedbackData)->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func onBtnDismiss(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onBtnDone(_ sender: UIButton) {
        self.dismiss(animated: true) {
            let rating = "\(Int(self.ratingView.rating))"
            let comment = self.tvComment.text ?? ""
            let feedback = FeedbackData(rating: rating, comment: comment)
            self.feedbackSubmitted?(feedback)
        }
    }
}
