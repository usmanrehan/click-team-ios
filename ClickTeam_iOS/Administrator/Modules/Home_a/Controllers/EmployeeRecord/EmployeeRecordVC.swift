//
//  EmployeeRecord.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 10/06/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit

class EmployeeRecordVC: BaseController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Employee Record"
        // Do any additional setup after loading the view.
    }

    @IBAction func onBtnEarnings(_ sender: UIButton) {
        self.pushToUsersList(type: .earning)
    }
    @IBAction func onBtnAppraisal(_ sender: UIButton) {
        self.pushToUsersList(type: .appraisal)
    }
    @IBAction func onBtnComparasion(_ sender: UIButton) {
        self.pushToComparasion()
    }

    
}
extension EmployeeRecordVC{
    private func pushToUsersList(type:CategoryType){
        let storyboard = AppStoryboard.Home_a.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "UsersList") as! UsersList
        controller.categoryType = type
        self.navigationController?.pushViewController(controller, animated: true)
    }
    private func pushToComparasion(){
        let storyboard = AppStoryboard.Home_a.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Comparasion")
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
