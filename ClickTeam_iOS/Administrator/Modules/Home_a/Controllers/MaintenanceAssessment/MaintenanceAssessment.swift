//
//  MaintenanceAssessment.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 10/06/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import ObjectMapper

enum MaintenanceStatus{
    case inProgress
    case completed
}

class MaintenanceAssessment: BaseController {

    @IBOutlet weak var btnInProgress: BlueWhiteButton!
    @IBOutlet weak var btnCompleted: BlueWhiteButton!
    @IBOutlet weak var tableView: UITableView!
    
    var maintenanceStatus = MaintenanceStatus.inProgress
    var arrMaintenanceInProgress = [JobModel]()
    var arrMaintenanceCompleted = [JobModel]()
    var selectedAcknowledgeJob:JobModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Maintenance Assessments"
        self.registerCell()
        self.addTaskPlusBarButtonItem()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        switch self.maintenanceStatus{
        case .inProgress:
            self.getMaintenanceInProgress()
        case .completed:
            self.getMaintenanceComplete()
        }
    }
    
    @IBAction func onBtnInProgress(_ sender: BlueWhiteButton) {
        if sender.isSelected {return}
        self.btnInProgress.isSelected = true
        self.btnCompleted.isSelected = false
        self.maintenanceStatus = .inProgress
        self.getMaintenanceInProgress()
    }
    @IBAction func onBtnInCompleted(_ sender: BlueWhiteButton) {
        if sender.isSelected {return}
        self.btnInProgress.isSelected = false
        self.btnCompleted.isSelected = true
        self.maintenanceStatus = .completed
        self.getMaintenanceComplete()
    }
    override func onBtnAddTask() {
        self.pushToUsersList(type: .addMaintenance)
    }
}
extension MaintenanceAssessment{
    private func registerCell(){
        self.tableView.register(UINib(nibName: "JobsListTVC", bundle: nil), forCellReuseIdentifier: "JobsListTVC")
        self.tableView.register(UINib(nibName: "JobAssessmentTVC", bundle: nil), forCellReuseIdentifier: "JobAssessmentTVC")
    }
    private func pushToUsersList(type:CategoryType){
        let storyboard = AppStoryboard.Home_a.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "UsersList") as! UsersList
        controller.categoryType = type
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @objc func onBtnAcknowledgeJob(sender:UIButton){
        Utility.main.showAlert(message: Strings.ASK_FOR_ACKNOWLEDGE_JOB.text, title: Strings.ACKNOWLEDGE_JOB.text, controller: self) { (yes, no) in
            if yes != nil{
                self.selectedAcknowledgeJob = self.arrMaintenanceInProgress[sender.tag]
                self.presentSubmitFeedback()
            }
        }
    }
    private func presentSubmitFeedback(){
        let storyboard = AppStoryboard.Home_a.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "FeedbackPopUp") as! FeedbackPopUp
        controller.feedbackSubmitted = {feedback in
            self.acknowledgeTask(feedback: feedback)
        }
        self.present(controller, animated: true, completion: nil)
    }
}
extension MaintenanceAssessment:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.maintenanceStatus {
        case .inProgress:
            return self.arrMaintenanceInProgress.count
        case .completed:
            return self.arrMaintenanceCompleted.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch self.maintenanceStatus {
        case .inProgress:
            let cell = tableView.dequeueReusableCell(withIdentifier: "JobAssessmentTVC", for: indexPath) as! JobAssessmentTVC
            let data = self.arrMaintenanceInProgress[indexPath.row]
            cell.setData(data: data)
            cell.btnAcknowledge.tag = indexPath.row
            cell.btnAcknowledge.addTarget(self, action: #selector(self.onBtnAcknowledgeJob(sender:)), for: .touchUpInside)
            cell.btnAcknowledge.isHidden = false
            return cell
        case .completed:
            let cell = tableView.dequeueReusableCell(withIdentifier: "JobAssessmentTVC", for: indexPath) as! JobAssessmentTVC
            let data = self.arrMaintenanceCompleted[indexPath.row]
            cell.setData(data: data)
            cell.btnAcknowledge.isHidden = true
            return cell
        }
    }
}
extension MaintenanceAssessment{
    private func getMaintenanceInProgress(){
        self.arrMaintenanceInProgress.removeAll()
        self.tableView.reloadData()
        let type = "2"
        let status = "30"
        let params:[String:Any] = ["type":type,"status":status]
        APIManager.sharedInstance.usersAPIManager.GetAllJobs(params: params, success: { (responseObject) in
            guard let maintenanceInProgress = responseObject as? [[String : Any]] else {return}
            self.arrMaintenanceInProgress = Mapper<JobModel>().mapArray(JSONArray: maintenanceInProgress)
            self.tableView.reloadData()
            if self.arrMaintenanceInProgress.isEmpty{
                Utility.main.showToast(message: "No in-progress maintenance available")
            }
        }) { (error) in
            print(error)
        }
    }
    private func getMaintenanceComplete(){
        self.arrMaintenanceCompleted.removeAll()
        self.tableView.reloadData()
        self.selectedAcknowledgeJob = nil
        let type = "2"
        let status = "40"
        let params:[String:Any] = ["type":type,"status":status]
        APIManager.sharedInstance.usersAPIManager.GetAllJobs(params: params, success: { (responseObject) in
            print(responseObject)
            guard let maintenanceInProgress = responseObject as? [[String : Any]] else {return}
            self.arrMaintenanceCompleted = Mapper<JobModel>().mapArray(JSONArray: maintenanceInProgress)
            self.tableView.reloadData()
            if self.arrMaintenanceCompleted.isEmpty{
                Utility.main.showToast(message: "No completed maintenance available")
            }
        }) { (error) in
            print(error)
        }
    }
    private func acknowledgeTask(feedback:FeedbackData){
        let task = self.selectedAcknowledgeJob
        let task_id = task?.id ?? "0"
        let status = "40"
        let rating = String(Int(feedback.rating) ?? 0)
        let comment_id = task?.comments.first?.id ?? "0"
        let params:[String:Any] = ["task_id":task_id,
                                   "status":status,
                                   "rating":rating,
                                   "comment_id":comment_id]
        APIManager.sharedInstance.usersAPIManager.taskComplete(params: params, success: { (responseObject) in
            print(responseObject)
            self.getMaintenanceInProgress()
        }) { (error) in
            print(error)
        }
    }
}
