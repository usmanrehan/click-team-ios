//
//  SenderMessage.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 17/06/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit

class SendMessage: UIViewController {

    @IBOutlet weak var lblEmployee: UILabel!
    @IBOutlet weak var tfSubject: UITextField!
    @IBOutlet weak var tvMessage: UITextView!
    var employee = EmployeeRecord()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.lblEmployee.text = self.employee.name ?? "-"
    }
    @IBAction func onBtnDismiss(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onBtnSendMessage(_ sender: UIButton) {
        self.validate(sender: sender)
    }
}
//MARK:- Helper Methods
extension SendMessage{
    private func validate(sender: UIButton){
        let subject = self.tfSubject.text ?? ""
        let message = self.tvMessage.text ?? ""
        if !Validation.validateStringLength(subject){
            Utility.main.showToast(message: "Subject is mendatory")
            return
        }
        if !Validation.validateStringLength(message){
            Utility.main.showToast(message: "Message is mendatory")
            return
        }
        self.sendMessage()
    }
}
//MARK:- Services
extension SendMessage{
    private func sendMessage(){
        let user_id = self.employee.id
        let subject = self.tfSubject.text ?? ""
        let message = self.tvMessage.text ?? ""
        let params:[String:Any] = ["send_to[]":user_id,
                                   "subject":subject,
                                   "message":message]
        APIManager.sharedInstance.usersAPIManager.sendMessage(params: params, success: { (responseObject) in
            self.dismiss(animated: true, completion: {
                Utility.main.showToast(message: "Message sent successfully")
            })
            print(responseObject)
        }) { (error) in
            print(error)
        }
    }
}
