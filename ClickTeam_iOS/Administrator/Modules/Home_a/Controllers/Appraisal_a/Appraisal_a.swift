//
//  Appraisal_a.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 11/06/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import ObjectMapper

class Appraisal_a: BaseController {
    
    @IBOutlet weak var tfDate: UITextField!
    @IBOutlet weak var tfPunctuality: UITextField!
    @IBOutlet weak var tfCustomerService: UITextField!
    @IBOutlet weak var tfRespectForPolicy: UITextField!
    @IBOutlet weak var tfInitiationLeaderShip: UITextField!
    @IBOutlet weak var tfTeamWork: UITextField!
    @IBOutlet weak var tfJobAcknowledge: UITextField!
    @IBOutlet weak var tfJobAttitude: UITextField!
    @IBOutlet weak var tfOthers: UITextField!
    
    var user: EmployeeRecord?
    var selectedDate = Date()
    var appraisals: AppraisalModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Appraisal"
        // Do any additional setup after loading the view.
        self.tfDate.delegate = self
        self.setDate(date: selectedDate)
        self.getEmployeeAppraisal()
    }
    @IBAction func onTfSelectDate(_ sender: UITextField) {
        self.getPickedDate(sender)
    }
}
//MARK:- Helper Methods
extension Appraisal_a{
    private func setDate(date:Date){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM yyyy"
        self.tfDate.text = dateFormatter.string(from: date)
        self.selectedDate = date
    }
    private func getSelectedDate()->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-yyyy"
        return dateFormatter.string(from: self.selectedDate)
    }
    private func setData(){
        if let data = self.appraisals{
            self.tfPunctuality.text = "\(data.punctuality)"
            self.tfCustomerService.text = "\(data.customerServices)"
            self.tfRespectForPolicy.text = "\(data.respect)"
            self.tfInitiationLeaderShip.text = "\(data.initiation)"
            self.tfTeamWork.text = "\(data.teamwork)"
            self.tfJobAcknowledge.text = "\(data.jobKnowledge)"
            self.tfJobAttitude.text = "\(data.jobAttitude)"
            self.tfOthers.text = "\(data.other)"
        }
        else{
            let pending = "Pending"
            self.tfPunctuality.text = pending
            self.tfCustomerService.text = pending
            self.tfRespectForPolicy.text = pending
            self.tfInitiationLeaderShip.text = pending
            self.tfTeamWork.text = pending
            self.tfJobAcknowledge.text = pending
            self.tfJobAttitude.text = pending
            self.tfOthers.text = pending
        }
    }
}
//MARK:- Date picker
extension Appraisal_a{
    private func getPickedDate(_ sender: UITextField){
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = .date
        datePickerView.maximumDate = Date()
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: .valueChanged)
    }
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        self.setDate(date: sender.date)
    }
}
//MARK:- UITextFieldDelegate
extension Appraisal_a:UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.getEmployeeAppraisal()
    }
}
//MARK:- Services
extension Appraisal_a{
    private func getEmployeeAppraisal(){
        let user_id = self.user?.id ?? 0
        let month = self.getSelectedDate()
        let search = "month:\(month)"
        let params:[String:Any] = ["search":search,"user_id":user_id]
        print(params)
        APIManager.sharedInstance.usersAPIManager.GetAppraisalReport(params: params, success: { (responseObject) in
            print(responseObject)
            guard let appraisal = responseObject as? [[String : Any]] else {return}
            let records = Mapper<AppraisalModel>().mapArray(JSONArray: appraisal)
            self.appraisals = records.last
            self.setData()
        }) { (error) in
            print(error)
        }
    }
}
