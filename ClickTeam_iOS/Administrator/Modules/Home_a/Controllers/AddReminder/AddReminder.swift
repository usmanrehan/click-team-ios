//
//  AddReminder.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 17/06/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit

struct ReminderData {
    var date:String
    var title:String
    var description:String
}

class AddReminder: UIViewController {

    @IBOutlet weak var tfDate: UITextField!
    @IBOutlet weak var tfTitle: UITextField!
    @IBOutlet weak var tvDescription: UITextView!
    
    var reminderSubmitted: ((ReminderData)->Void)?
    var selectedDate = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setDate(date: Date())
    }
    
    @IBAction func onTfDatePicker(_ sender: UITextField) {
        self.getPickedDate(sender)
    }
    
    @IBAction func onBtnDismiss(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onBtnSubmit(_ sender: UIButton) {
        self.dismiss(animated: true) {
            let date = self.getDate()
            let title = self.tfTitle.text ?? ""
            let description = self.tvDescription.text ?? ""
            let reminder = ReminderData(date: date, title: title, description: description)
            self.reminderSubmitted?(reminder)
        }
    }
}
//MARK:- Helper Methods
extension AddReminder{
    private func setDate(date:Date){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd, MMM yyyy"
        self.tfDate.text = dateFormatter.string(from: date)
        self.selectedDate = date
    }
    private func getDate()->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: self.selectedDate)
    }
}
//MARK:- Date picker
extension AddReminder{
    private func getPickedDate(_ sender: UITextField){
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = .date
        datePickerView.minimumDate = Date()
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: .valueChanged)
    }
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        self.setDate(date: sender.date)
    }
}
