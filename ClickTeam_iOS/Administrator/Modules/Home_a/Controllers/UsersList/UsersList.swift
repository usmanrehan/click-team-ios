//
//  UsersList.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 19/05/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import ObjectMapper

enum CategoryType{
    case assignDuties
    case dailyAssessments
    case allUsers
    case employeeReport
    case addMaintenance
    case earning
    case appraisal
    case editUserDetail
    case reminders
    case sendMessage
}

class UsersList: BaseController {

    @IBOutlet weak var tableView: UITableView!
    var categoryType = CategoryType.assignDuties
    var arrEmployees = [EmployeeRecord]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Select User"
        self.registerCell()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getAllEmployees()
    }
}
//MARK:- Helper Methods
extension UsersList{
    private func registerCell(){
        self.tableView.register(UINib(nibName: "UsersListTVC", bundle: nil), forCellReuseIdentifier: "UsersListTVC")
    }
    private func pushToJobsList(employee:EmployeeRecord){
        let storyboard = AppStoryboard.Home_a.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "JobsList") as! JobsList
        controller.employee = employee
        self.navigationController?.pushViewController(controller, animated: true)
    }
    private func pushToJobAssessment(user:EmployeeRecord){
        let storyboard = AppStoryboard.Home_a.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "JobAssessment") as! JobAssessment
        controller.employee = user
        self.navigationController?.pushViewController(controller, animated: true)
    }
    private func pushToEarningReport(user:EmployeeRecord){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Earning") as! Earning
        controller.user = user
        self.navigationController?.pushViewController(controller, animated: true)
    }
    private func pushToAppraisalReport(user:EmployeeRecord){
        let storyboard = AppStoryboard.Home_a.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Appraisal_a") as! Appraisal_a
        controller.user = user
        self.navigationController?.pushViewController(controller, animated: true)
    }
    private func pushToUserDetail(user:EmployeeRecord){
        let storyboard = AppStoryboard.Home_a.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "UserDetail") as! UserDetail
        controller.employee = user
        self.navigationController?.pushViewController(controller, animated: true)
    }
    private func pushToAddTask(user:EmployeeRecord){
        let storyboard = AppStoryboard.Home_a.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "AddTask") as! AddTask
        controller.employee = user
        controller.category = .conformance
        self.navigationController?.pushViewController(controller, animated: true)
    }
    private func pushToReminders(user:EmployeeRecord){
        let storyboard = AppStoryboard.Home_a.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Reminders") as! Reminders
        controller.employee = user
        self.navigationController?.pushViewController(controller, animated: true)
    }
    private func presentSendMessage(user:EmployeeRecord){
        let storyboard = AppStoryboard.Home_a.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "SendMessage") as! SendMessage
        controller.employee = user
        self.present(controller, animated: true, completion: nil)
    }
}
extension UsersList:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrEmployees.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UsersListTVC", for: indexPath) as! UsersListTVC
        cell.setData(data: self.arrEmployees[indexPath.row])
        return cell
    }
}
extension UsersList:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch self.categoryType {
        case .assignDuties:
            let employee = self.arrEmployees[indexPath.row]
            self.pushToJobsList(employee: employee)
        case .dailyAssessments:
            let user = self.arrEmployees[indexPath.row]
            self.pushToJobAssessment(user: user)
        case .allUsers:
            break
        case .employeeReport:
            break
        case .addMaintenance:
            let user = self.arrEmployees[indexPath.row]
            self.pushToAddTask(user: user)
        case .earning:
            let user = self.arrEmployees[indexPath.row]
            self.pushToEarningReport(user: user)
        case .appraisal:
            let user = self.arrEmployees[indexPath.row]
            self.pushToAppraisalReport(user: user)
        case .editUserDetail:
            let user = self.arrEmployees[indexPath.row]
            self.pushToUserDetail(user: user)
        case .reminders:
            let user = self.arrEmployees[indexPath.row]
            self.pushToReminders(user: user)
        case .sendMessage:
            let user = self.arrEmployees[indexPath.row]
            self.presentSendMessage(user: user)
        }
    }
}
//MARK:- Services
extension UsersList{
    private func getAllEmployees(){
        APIManager.sharedInstance.usersAPIManager.GetAllEmployees(params: [:], success: { (responseObject) in
            print(responseObject)
            guard let employees = responseObject as? [[String : Any]] else {return}
            self.arrEmployees = Mapper<EmployeeRecord>().mapArray(JSONArray: employees)
            self.tableView.reloadData()
            if self.arrEmployees.isEmpty{
                Utility.main.showToast(message: "No employee available")
            }
        }) { (error) in
            print(error.localizedDescription)
        }
    }
}
