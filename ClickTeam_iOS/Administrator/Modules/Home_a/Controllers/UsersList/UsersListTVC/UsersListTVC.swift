//
//  UsersListTVC.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 19/05/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import SDWebImage
import Cosmos

class UsersListTVC: UITableViewCell {

    @IBOutlet weak var imgProfile: RoundedImage!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDesignation: UILabel!
    @IBOutlet weak var viewRatings: CosmosView!
    
    func setData(data:EmployeeRecord){
        if let profileImage = data.details?.image{
            if let profileImageURL = URL(string: profileImage){
                self.imgProfile.sd_setImage(with: profileImageURL, completed: nil)
            }
        }
        self.lblName.text = data.name ?? "-"
        self.lblDesignation.text = data.roles.first?.displayName ?? "-"
        self.viewRatings.rating = data.ratings
    }
    
}
