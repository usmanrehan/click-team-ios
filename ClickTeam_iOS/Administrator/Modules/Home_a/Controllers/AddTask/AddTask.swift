//
//  AddTask.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 20/05/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import ObjectMapper
import DropDown

enum AddTaskWithCategoryType{
    case performance
    case conformance
}

class AddTask: BaseController {

    @IBOutlet weak var tfJobTitle: UITextField!
    @IBOutlet weak var tfJobDescription: UITextField!
    @IBOutlet weak var tfPointValue: UITextField!
    @IBOutlet weak var tfCashValue: UITextField!
    @IBOutlet weak var tfRole: UITextField!
    @IBOutlet weak var tfStartDate: UITextField!
    @IBOutlet weak var tfEndDate: UITextField!
    
    var startDate = ""
    var endDate = ""
    var employee = EmployeeRecord()
    var arrRoleModel = [Roles]()
    var selectedRole: Roles?
    let dropDown = DropDown()
    var newTask = JobModel()
    var taskToAssign: JobModel?
    
    var category = AddTaskWithCategoryType.performance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Add Task"
        self.tfRole.delegate = self
        //self.getRoles()
        // Do any additional setup after loading the view.
        self.setDate(date: Date(), tag: 0)
        self.setDate(date: Date(), tag: 1)
        self.setData()
    }
    
    @IBAction func onTfDateSelect(_ sender: UITextField) {
        self.getPickedDate(sender)
    }
    
    @IBAction func onBtnSubmit(_ sender: UIButton) {
        if self.taskToAssign != nil{
            let jobDescription = self.tfJobDescription.text ?? ""
            if !Validation.validateStringLength(jobDescription){
                Utility.main.showToast(message: "Please enter job description.")
                sender.shake()
                return
            }
            self.assigTaskWithTaskID()
        }
        else{
            self.validate(sender: sender)
        }
    }
}
//MARK:- Helper Methods
extension AddTask{
    private func setDropDownRoles(){
        self.dropDown.anchorView = self.tfRole
        self.dropDown.direction = .top
        var roles = [String]()
        for item in self.arrRoleModel{
            roles.append(item.displayName ?? "-")
        }
        self.dropDown.dataSource = roles
        self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            let data = self.arrRoleModel[index]
            self.setRole(data: data)
        }
    }
    private func setRole(data:Roles){
        self.selectedRole = data
        let displayName = data.displayName ?? "-"
        self.tfRole.text = displayName
    }
    private func validate(sender:UIButton){
        let jobTitle = self.tfJobTitle.text ?? ""
        let jobDescription = self.tfJobDescription.text ?? ""
        let pointValue = self.tfPointValue.text ?? ""
        let castValue = self.tfCashValue.text ?? ""
        let role = self.tfRole.text ?? ""
       
        if !Validation.validateStringLength(jobTitle){
            Utility.main.showToast(message: "Please enter job title.")
            sender.shake()
            return
        }
        if !Validation.validateStringLength(jobDescription){
            Utility.main.showToast(message: "Please enter job description.")
            sender.shake()
            return
        }
        if !Validation.validateStringLength(pointValue){
            Utility.main.showToast(message: "Please enter point value.")
            sender.shake()
            return
        }
        if !Validation.validateStringLength(castValue){
            Utility.main.showToast(message: "Please enter cash value.")
            sender.shake()
            return
        }
        if !Validation.validateStringLength(role){
            Utility.main.showToast(message: "Please select a role.")
            sender.shake()
            return
        }
        self.addNewTaskCategory()
    }
    private func setData(){
        self.tfRole.isUserInteractionEnabled = false
        self.tfRole.text = self.employee.roles.first?.name
        guard let data = self.taskToAssign else {return}
        self.tfJobTitle.isUserInteractionEnabled = false
        self.tfJobTitle.text = data.title
        self.tfCashValue.text = data.cashValue
        self.tfPointValue.text = data.earnPoint
    }
}
//MARK:- Helper Methods
extension AddTask{
    private func setDate(date:Date,tag:Int){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd,MMM yyyy"
        if tag == 0{
            self.tfStartDate.text = dateFormatter.string(from: date)
            dateFormatter.dateFormat = "yyyy-MM-dd h:mm:ss"
            self.startDate = dateFormatter.string(from: date)
        }
        else{
            self.tfEndDate.text = dateFormatter.string(from: date)
            dateFormatter.dateFormat = "yyyy-MM-dd h:mm:ss"
            self.endDate = dateFormatter.string(from: date)
        }
    }
}
//MARK:- Date picker
extension AddTask{
    private func getPickedDate(_ sender: UITextField){
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = .date
        datePickerView.minimumDate = Date()
        sender.inputView = datePickerView
        datePickerView.tag = sender.tag
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: .valueChanged)
    }
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        self.setDate(date: sender.date, tag: sender.tag)
    }
}
//MARK:- Helper Methods
extension AddTask:UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if self.arrRoleModel.isEmpty{
            self.getRoles()
            return false
        }
        self.dropDown.show()
        return false
    }
}
//MARK:- Services
extension AddTask{
    private func addNewTaskCategory(){
        var parent_task_id = ""
        switch self.category {
        case .performance:
            parent_task_id = "1"
        case .conformance:
            parent_task_id = "2"
        }
        let title = self.tfJobTitle.text ?? ""
        let detail = self.tfJobDescription.text ?? ""
        let earn_point = self.tfPointValue.text ?? ""
        let cash_value = self.tfCashValue.text ?? ""
        let start_date = self.startDate
        let end_date = self.endDate
        let priority = "10"
        let status = "10"
        let is_already_assigned = "0"
        let roles = self.employee.id
        //let role_id = self.selectedRole?.id ?? 0
        //let created_by = AppStateManager.sharedInstance.loggedInUser.id
        let params:[String:Any] = ["parent_task_id":parent_task_id,
                                   "title":title,
                                   "detail":detail,
                                   "earn_point":earn_point,
                                   "cash_value":cash_value,
                                   "start_date":start_date,
                                   "end_date":end_date,
                                   "priority":priority,
                                   "status":status,
                                   "created_by":"1",
                                   "is_already_assigned":is_already_assigned,
                                   "roles[]":roles,]
        print(params)
        APIManager.sharedInstance.usersAPIManager.addTaskWithCategory(params: params, success: { (responseObject) in
            print(responseObject)
            self.newTask = Mapper<JobModel>().map(JSON: responseObject) ?? JobModel()
            switch self.category{
            case .performance:
                Utility.main.showAlert(message: "Task assigned successfully", title: "Task assigned", controller: self, usingCompletionHandler: {
                    self.navigationController?.popViewController(animated: true)
                })
            case .conformance:
                Utility.main.showAlert(message: "Task assigned successfully", title: "Task assigned", controller: self, usingCompletionHandler: {
                    self.navigationController?.popViewController(animated: true)
                })
            }
        }) { (error) in
            print(error)
        }
    }
    private func getRoles(){
        APIManager.sharedInstance.usersAPIManager.GetRoles(params: [:], success: { (responseObject) in
            print(responseObject)
            guard let roles = responseObject as? [[String : Any]] else {return}
            self.arrRoleModel = Mapper<Roles>().mapArray(JSONArray: roles)
            self.setDropDownRoles()
        }) { (error) in
            print(error)
        }
    }
    private func assignTask(){
        let task_id = self.newTask.taskID
        let detail = self.tfJobDescription.text ?? ""
        let earn_point = self.tfPointValue.text ?? ""
        let cash_value = self.tfCashValue.text ?? ""
        let start_date = self.startDate
        let end_date = self.endDate
        let priority = "10"
        let status = "10"
        let is_already_assigned = "1"
        let roles = self.employee.id
        let created_by = AppStateManager.sharedInstance.loggedInUser.id
        
        let params:[String:Any] = ["task_id":task_id,
                                   "detail":detail,
                                   "earn_point":earn_point,
                                   "cash_value":cash_value,
                                   "start_date":start_date,
                                   "end_date":end_date,
                                   "priority":priority,
                                   "status":status,
                                   "is_already_assigned":is_already_assigned,
                                   "roles[]":roles,
                                   "created_by":created_by]
        print(params)
        APIManager.sharedInstance.usersAPIManager.AddTaskWithCategoryID(params: params, success: { (responseObject) in
            Utility.main.showAlert(message: "Task assigned successfully", title: "Task assigned", controller: self, usingCompletionHandler: {
                self.navigationController?.popViewController(animated: true)
            })
        }) { (error) in
            print(error)
        }
    }
//    private func assigTaskWithTaskID(){
//        let task_id = self.taskToAssign?.ID ?? 0
//        let is_already_assigned = "1"
//        let user_id = self.employee.id
//        let params:[String:Any] = ["task_id":task_id,
//                                   "is_already_assigned":is_already_assigned,
//                                   "user_id":user_id]
//        print(params)
//        APIManager.sharedInstance.usersAPIManager.AssignUnAssignTask(params: params, success: { (responseObject) in
//            Utility.main.showAlert(message: "Task assigned successfully", title: "Task assigned", controller: self, usingCompletionHandler: {
//                self.navigationController?.popViewController(animated: true)
//            })
//        }) { (error) in
//            print(error)
//        }
//    }
    private func assigTaskWithTaskID(){
        let task_id = self.taskToAssign?.ID ?? 0
        let detail = self.tfJobDescription.text ?? ""
        let earn_point = self.tfPointValue.text ?? ""
        let cash_value = self.tfCashValue.text ?? ""
        let start_date = self.startDate
        let end_date = self.endDate
        let priority = "10"
        let status = "10"
        let is_already_assigned = "1"
        let roles = self.employee.id
        let created_by = AppStateManager.sharedInstance.loggedInUser.id
        
        let params:[String:Any] = ["task_id":task_id,
                                   "detail":detail,
                                   "earn_point":earn_point,
                                   "cash_value":cash_value,
                                   "start_date":start_date,
                                   "end_date":end_date,
                                   "priority":priority,
                                   "status":status,
                                   "is_already_assigned":is_already_assigned,
                                   "roles[]":roles,
                                   "created_by":created_by]
        print(params)
        APIManager.sharedInstance.usersAPIManager.AddTaskWithCategoryID(params: params, success: { (responseObject) in
            Utility.main.showAlert(message: "Task assigned successfully", title: "Task assigned", controller: self, usingCompletionHandler: {
                self.navigationController?.popViewController(animated: true)
            })
        }) { (error) in
            print(error)
        }
    }
}
