//
//  JobsListTVC.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 20/05/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit

class JobsListTVC: UITableViewCell {

    @IBOutlet weak var lblJobTitle: UILabel!
    @IBOutlet weak var lblJobDescription: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnAssignJob: BlueWhiteBorderButton!
    
    func setData(data:JobModel){
        self.lblJobTitle.text = data.title ?? "-"
        self.lblJobDescription.text = data.detail ?? ""
        self.lblDate.text = data.startDate ?? "-"
//        let assigneeName = data.
//        self.btnAssignJob
    }
    
}
