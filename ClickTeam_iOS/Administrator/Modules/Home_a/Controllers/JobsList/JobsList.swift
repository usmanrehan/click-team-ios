//
//  JobsList.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 20/05/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import ObjectMapper

enum JobStatus{
    case assignedJobs
    case allJobs
}

class JobsList: BaseController {

    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var btnAssignedJobs: BlueWhiteButton!
    @IBOutlet weak var btnAllJobs: BlueWhiteButton!
    @IBOutlet weak var tableView: UITableView!
    
    var employee = EmployeeRecord()
    var arrJobs = [JobModel]()
    var arrFilteredJobs = [JobModel]()
    var arrAssignedJobs = [JobModel]()
    var isFilter = false
    var selectedJobModel: JobModel?
    var jobStatus = JobStatus.assignedJobs
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let userName = self.employee.name ?? ""
        self.title = "\(userName) Jobs"
        self.registerCell()
        self.setSearchBoxUI()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getAllAssignedJobs()
        self.getAllJobs()
    }
    override func onBtnAddTask() {
        self.pushToAddTask(user: self.employee, taskToAssign: nil)
    }
    
    @IBAction func onTfSearch(_ sender: UITextField) {
        let search = sender.text ?? ""
        if search.isEmpty{
            self.isFilter = false
        }
        else{
            self.isFilter = true
            self.arrFilteredJobs = self.arrJobs.filter { $0.title!.localizedCaseInsensitiveContains(search) }
        }
        self.tableView.reloadData()
    }
    @IBAction func onBtnAssignedJobs(_ sender: BlueWhiteButton) {
        if sender.isSelected {return}
        self.btnAssignedJobs.isSelected = true
        self.btnAllJobs.isSelected = false
        self.jobStatus = .assignedJobs
        self.setSearchBoxUI()
        self.tableView.reloadData()
        self.getAllAssignedJobs()
    }
    @IBAction func onBtnAllJobs(_ sender: BlueWhiteButton) {
        if sender.isSelected {return}
        self.btnAssignedJobs.isSelected = false
        self.btnAllJobs.isSelected = true
        self.jobStatus = .allJobs
        self.setSearchBoxUI()
        self.tableView.reloadData()
    }
}
//MARK:- Helper Methods
extension JobsList{
    private func registerCell(){
        self.tableView.register(UINib(nibName: "JobsListTVC", bundle: nil), forCellReuseIdentifier: "JobsListTVC")
    }
    @objc func onBtnChangeJobStatus(sender:UIButton){
        var data = JobModel()
        switch self.jobStatus{
        case .assignedJobs:
            data = self.arrAssignedJobs[sender.tag]
        case .allJobs:
            if !self.isFilter{
                data = self.arrJobs[sender.tag]
            }
            else{
                data = self.arrFilteredJobs[sender.tag]
            }
        }
        
        self.selectedJobModel = data
        let employeeName = self.employee.name ?? "-"
        if sender.isSelected{
            Utility.main.showAlert(message: Strings.ASK_FOR_RESIGN_JOB.text + "\(employeeName)", title: Strings.CANCEL_ASSIGN_JOB.text, controller: self) { (yes, no) in
                if yes != nil{
                    self.unAssignTask()
                }
            }
        }
        else{
            Utility.main.showAlert(message: Strings.ASK_FOR_ASSIGN_JOB.text + "\(employeeName)", title: Strings.ASSIGN_JOB.text, controller: self) { (yes, no) in
                if yes != nil{
                    self.pushToAddTask(user: self.employee, taskToAssign: data)
                }
            }
        }
    }
    private func pushToAddTask(user:EmployeeRecord,taskToAssign:JobModel?){
        let storyboard = AppStoryboard.Home_a.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "AddTask") as! AddTask
        controller.employee = user
        controller.taskToAssign = taskToAssign
        self.navigationController?.pushViewController(controller, animated: true)
    }
    private func setSearchBoxUI(){
        switch self.jobStatus{
        case .assignedJobs:
            self.searchView.isHidden = true
        case .allJobs:
            self.searchView.isHidden = false
        }
    }
}
extension JobsList:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.jobStatus{
        case .assignedJobs:
            return self.arrAssignedJobs.count
        case .allJobs:
            if !self.isFilter{
                return self.arrJobs.count
            }
            else{
                return self.arrFilteredJobs.count
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch self.jobStatus{
        case .assignedJobs:
            let cell = tableView.dequeueReusableCell(withIdentifier: "JobsListTVC", for: indexPath) as! JobsListTVC
            let data = self.arrAssignedJobs[indexPath.row]
            let employeeName = self.employee.name ?? "-"
            cell.setData(data: data)
            cell.btnAssignJob.tag = indexPath.row
            cell.btnAssignJob.addTarget(self, action: #selector(self.onBtnChangeJobStatus(sender:)), for: .touchUpInside)
            cell.btnAssignJob.isSelected = true
            cell.btnAssignJob.setTitle("Assigned to \(employeeName)", for: .selected)
            return cell
        case .allJobs:
            if !self.isFilter{
                let cell = tableView.dequeueReusableCell(withIdentifier: "JobsListTVC", for: indexPath) as! JobsListTVC
                let data = self.arrJobs[indexPath.row]
                let employeeName = self.employee.name ?? "-"
                cell.setData(data: data)
                cell.btnAssignJob.tag = indexPath.row
                cell.btnAssignJob.addTarget(self, action: #selector(self.onBtnChangeJobStatus(sender:)), for: .touchUpInside)
                cell.btnAssignJob.isSelected = false
                cell.btnAssignJob.setTitle("Assign to \(employeeName)", for: .normal)
                return cell
            }
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "JobsListTVC", for: indexPath) as! JobsListTVC
                let data = self.arrFilteredJobs[indexPath.row]
                let employeeName = self.employee.name ?? "-"
                cell.setData(data: data)
                cell.btnAssignJob.tag = indexPath.row
                cell.btnAssignJob.addTarget(self, action: #selector(self.onBtnChangeJobStatus(sender:)), for: .touchUpInside)
                cell.btnAssignJob.isSelected = false
                cell.btnAssignJob.setTitle("Assign to \(employeeName)", for: .normal)
                return cell
            }
        }
        
    }
}
extension JobsList{
    private func getAllJobs(){
        let parent_task_id = "1"
        let params:[String:Any] = ["parent_task_id":parent_task_id]
        APIManager.sharedInstance.usersAPIManager.GetTaskCategories(params: params, success: { (responseObject) in
            print(responseObject)
            guard let jobs = responseObject as? [[String : Any]] else {return}
            self.arrJobs = Mapper<JobModel>().mapArray(JSONArray: jobs)
            print(self.arrJobs)
            self.tableView.reloadData()
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    private func getAllAssignedJobs(){
        let type = "1"
        let status = "10"
        let userId = self.employee.id
        let params:[String:Any] = ["type":type,"user_id":userId,"status":status]
        APIManager.sharedInstance.usersAPIManager.GetAllJobs(params: params, success: { (responseObject) in
            guard let jobs = responseObject as? [[String : Any]] else {return}
            self.arrAssignedJobs = Mapper<JobModel>().mapArray(JSONArray: jobs)
            self.tableView.reloadData()
            if self.arrAssignedJobs.isEmpty{
                Utility.main.showToast(message: "No record available")
            }
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    private func assignTask(){
        let task_id = self.selectedJobModel?.ID ?? 0
        let is_already_assigned = "1"
        let user_id = self.employee.id
        let params:[String:Any] = ["task_id":task_id,
                                   "is_already_assigned":is_already_assigned,
                                   "user_id":user_id]
        APIManager.sharedInstance.usersAPIManager.AssignUnAssignTask(params: params, success: { (responseObject) in
            //self.getAllJobs()
        }) { (error) in
            print(error)
        }
    }
    private func unAssignTask(){
        let task_id = self.selectedJobModel?.id ?? "0"
        let is_already_assigned = "0"
        let user_id = self.employee.id
        let params:[String:Any] = ["task_id":task_id,
                                   "is_already_assigned":is_already_assigned,
                                   "user_id":user_id]
        APIManager.sharedInstance.usersAPIManager.AssignUnAssignTask(params: params, success: { (responseObject) in
            self.getAllAssignedJobs()
        }) { (error) in
            print(error)
        }
    }
}
