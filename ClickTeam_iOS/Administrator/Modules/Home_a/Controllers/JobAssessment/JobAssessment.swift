//
//  JobAssessment.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 22/05/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import ObjectMapper

enum AssessmentType{
    case assessment
    case appraisal
}

class JobAssessment: BaseController {

    @IBOutlet weak var btnAssessment: BlueWhiteButton!
    @IBOutlet weak var btnAppraisal: BlueWhiteButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewAppraisal: UIView!
    @IBOutlet weak var tfPunctuality: UITextField!
    @IBOutlet weak var tfCustomerService: UITextField!
    @IBOutlet weak var tfRespectForPolicy: UITextField!
    @IBOutlet weak var tfInitiationLeaderShip: UITextField!
    @IBOutlet weak var tfTeamWork: UITextField!
    @IBOutlet weak var tfJobAcknowledge: UITextField!
    @IBOutlet weak var tfJobAttitude: UITextField!
    @IBOutlet weak var tfOthers: UITextField!
    @IBOutlet weak var btnDoneAppraisal: UIButton!
    
    var assessmentType = AssessmentType.assessment
    var appraisals: AppraisalModel?
    var employee = EmployeeRecord()
    var arrDailyAssessment = [JobModel]()
    var selectedDailyAssessment:JobModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Job Assessment"
        self.setUI()
        self.registerCell()
        self.getDailyAssessmentInProgress()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnAssessment(_ sender: BlueWhiteButton) {
        self.selectAssessmentButton()
    }
    @IBAction func onBtnAppraisal(_ sender: BlueWhiteButton) {
        self.selectAppraisalButton()
        self.getEmployeeAppraisal()
    }
    @IBAction func onBtnDoneAppraisal(_ sender: UIButton) {
        self.setAppraisal()
    }
}
//MARK:- Helper Methods
extension JobAssessment{
    private func setUI(){
        switch self.assessmentType{
        case .assessment:
            self.viewAppraisal.isHidden = true
        case .appraisal:
            self.viewAppraisal.isHidden = false
        }
    }
    private func registerCell(){
        self.tableView.register(UINib(nibName: "JobAssessmentTVC", bundle: nil), forCellReuseIdentifier: "JobAssessmentTVC")
    }
    private func selectAssessmentButton(){
        if self.btnAssessment.isSelected{return}
        self.btnAssessment.isSelected = true
        self.btnAppraisal.isSelected = false
        self.assessmentType = .assessment
        self.setUI()
    }
    private func selectAppraisalButton(){
        if self.btnAppraisal.isSelected{return}
        self.btnAppraisal.isSelected = true
        self.btnAssessment.isSelected = false
        self.assessmentType = .appraisal
        self.setUI()
    }
    private func presentSubmitFeedback(){
        let storyboard = AppStoryboard.Home_a.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "FeedbackPopUp") as! FeedbackPopUp
        controller.feedbackSubmitted = {feedback in
            self.acknowledgeTask(feedback: feedback)
        }
        self.present(controller, animated: true, completion: nil)
    }
    @objc func onBtnAcknowledgeJob(sender:UIButton){
        Utility.main.showAlert(message: Strings.ASK_FOR_ACKNOWLEDGE_JOB.text, title: Strings.ACKNOWLEDGE_JOB.text, controller: self) { (yes, no) in
            if yes != nil{
                self.selectedDailyAssessment = self.arrDailyAssessment[sender.tag]
                self.presentSubmitFeedback()
            }
        }
    }
    private func setData(){
        if let data = self.appraisals{
            self.tfPunctuality.text = "\(data.punctuality)"
            self.tfCustomerService.text = "\(data.customerServices)"
            self.tfRespectForPolicy.text = "\(data.respect)"
            self.tfInitiationLeaderShip.text = "\(data.initiation)"
            self.tfTeamWork.text = "\(data.teamwork)"
            self.tfJobAcknowledge.text = "\(data.jobKnowledge)"
            self.tfJobAttitude.text = "\(data.jobAttitude)"
            self.tfOthers.text = "\(data.other)"
        }
        else{
            let pending = "Pending"
            self.tfPunctuality.placeholder = pending
            self.tfCustomerService.placeholder = pending
            self.tfRespectForPolicy.placeholder = pending
            self.tfInitiationLeaderShip.placeholder = pending
            self.tfTeamWork.placeholder = pending
            self.tfJobAcknowledge.placeholder = pending
            self.tfJobAttitude.placeholder = pending
            self.tfOthers.placeholder = pending
        }
    }
    private func getDate()->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-yyyy"
        return dateFormatter.string(from: Date())
    }
}
//MARK:- Helper Methods
extension JobAssessment:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrDailyAssessment.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "JobAssessmentTVC", for: indexPath) as! JobAssessmentTVC
//        cell.btnAcknowledge.tag = indexPath.row
//        cell.btnAcknowledge.addTarget(self, action: #selector(self.onBtnAcknowledgeJob(sender:)), for: .touchUpInside)
        let cell = tableView.dequeueReusableCell(withIdentifier: "JobAssessmentTVC", for: indexPath) as! JobAssessmentTVC
        let data = self.arrDailyAssessment[indexPath.row]
        cell.setData(data: data)
        cell.btnAcknowledge.tag = indexPath.row
        cell.btnAcknowledge.addTarget(self, action: #selector(self.onBtnAcknowledgeJob(sender:)), for: .touchUpInside)
        return cell
    }
}
extension JobAssessment{
    private func getDailyAssessmentInProgress(){
        self.arrDailyAssessment.removeAll()
        self.tableView.reloadData()
        let type = "1"
        let status = "30"
        let params:[String:Any] = ["type":type,"status":status]
        APIManager.sharedInstance.usersAPIManager.GetAllJobs(params: params, success: { (responseObject) in
            guard let maintenanceInProgress = responseObject as? [[String : Any]] else {return}
            self.arrDailyAssessment = Mapper<JobModel>().mapArray(JSONArray: maintenanceInProgress)
            self.tableView.reloadData()
            if self.arrDailyAssessment.isEmpty{
                Utility.main.showToast(message: "No record available")
            }
        }) { (error) in
            print(error)
        }
    }
    private func acknowledgeTask(feedback:FeedbackData){
        let task = self.selectedDailyAssessment
        let task_id = task?.id ?? "0"
        let status = "40"
        let rating = String(Int(feedback.rating) ?? 0)
        let comment_id = task?.comments.first?.id ?? "0"
        let params:[String:Any] = ["task_id":task_id,
                                   "status":status,
                                   "rating":rating,
                                   "comment_id":comment_id]
        APIManager.sharedInstance.usersAPIManager.taskComplete(params: params, success: { (responseObject) in
            print(responseObject)
            self.getDailyAssessmentInProgress()
        }) { (error) in
            print(error)
        }
    }
    private func getEmployeeAppraisal(){
        let user_id = self.employee.id
        let month = self.getDate()
        let search = "month:\(month)"
        let params:[String:Any] = ["search":search,"user_id":user_id]
        print(params)
        APIManager.sharedInstance.usersAPIManager.GetAppraisalReport(params: params, success: { (responseObject) in
            print(responseObject)
            guard let appraisal = responseObject as? [[String : Any]] else {return}
            let records = Mapper<AppraisalModel>().mapArray(JSONArray: appraisal)
            self.appraisals = records.last
            self.setData()
        }) { (error) in
            print(error)
        }
    }
    private func setAppraisal(){
        let user_id = self.employee.id
        let month = self.getDate()
        let punctuality = self.tfPunctuality.text ?? ""
        let customer_services = self.tfCustomerService.text ?? ""
        let respect = self.tfRespectForPolicy.text ?? ""
        let initiation = self.tfInitiationLeaderShip.text ?? ""
        let teamwork = self.tfTeamWork.text ?? ""
        let job_knowledge = self.tfJobAcknowledge.text ?? ""
        let job_attitude = self.tfJobAttitude.text ?? ""
        let other = self.tfOthers.text ?? ""
        let params:[String:Any] = ["user_id":user_id,
                                   "month":month,
                                   "punctuality":punctuality,
                                   "customer_services":customer_services,
                                   "respect":respect,
                                   "initiation":initiation,
                                   "teamwork":teamwork,
                                   "job_knowledge":job_knowledge,
                                   "job_attitude":job_attitude,
                                   "other":other]
        APIManager.sharedInstance.usersAPIManager.setAppraisal(params: params, success: { (responseObject) in
            print(responseObject)
            self.view.endEditing(true)
            self.getEmployeeAppraisal()
        }) { (error) in
            print(error)
        }
    }
}
