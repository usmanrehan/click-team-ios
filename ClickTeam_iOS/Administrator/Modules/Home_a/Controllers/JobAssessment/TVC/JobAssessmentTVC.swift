//
//  JobAssessmentTVC.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 22/05/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import RealmSwift
import Cosmos

class JobAssessmentTVC: UITableViewCell {

    @IBOutlet weak var lblJobTitle: UILabel!
    @IBOutlet weak var lblJobDescription: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblCarriedOut: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var viewRating: CosmosView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnAttachedDoc: UIButton!
    @IBOutlet weak var btnLink: UIButton!
    @IBOutlet weak var btnAcknowledge: UIButton!
    
    var currentJob = JobModel()
    var arrImages = List<Images>()
    var arrImagesUrl = [String]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.registerCVC()
    }

    @IBAction func onBtnAcknowledge(_ sender: UIButton) {
    }
    
}
//MARK:- Helper Methods
extension JobAssessmentTVC{
    func setData(data:JobModel){
        self.currentJob = data
        self.lblJobTitle.text = data.title ?? "-"
        self.lblJobDescription.text = data.detail ?? ""
        self.lblDate.text = data.startDate ?? "-"
        if (data.status ?? "") == "40"{
            self.lblStatus.text = "Completed"
        }
        let carriedOut = data.comments.first?.carriedOut ?? ""
        if carriedOut == "1"{
            self.lblCarriedOut.text = "Yes"
        }
        else{
            self.lblCarriedOut.text = "No"
        }
        let quantity = data.comments.first?.quantity ?? ""
        self.lblQuantity.text = quantity
        let comment = data.comments.first?.comment ?? ""
        self.lblComment.text = comment
        let rating = Double(data.comments.first?.rating ?? "0") ?? 0.0
        self.viewRating.rating = rating
        let file = data.comments.first?.file.first?.attachment ?? ""
        if !file.isEmpty{
            self.btnAttachedDoc.setTitle(file, for: .normal)
        }
        else{
            self.btnAttachedDoc.setTitle("No file attached", for: .normal)
        }
        self.arrImages = data.comments.first?.images ?? List<Images>()
        self.arrImagesUrl.removeAll()
        for image in self.arrImages{
            self.arrImagesUrl.append(image.attachment ?? "")
        }
        self.collectionView.reloadData()
    }
    private func registerCVC(){
        self.collectionView.register(UINib(nibName: "ImageCVC", bundle: nil), forCellWithReuseIdentifier: "ImageCVC")
    }
    private func pushToSlideShow(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "SlideShow") as! SlideShow
        controller.currentJob = self.currentJob
        controller.arrImages = self.arrImagesUrl
        Utility.main.topViewController()?.navigationController?.pushViewController(controller, animated: true)
    }
}
//MARK:- UICollectionViewDataSource
extension JobAssessmentTVC:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrImages.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCVC", for: indexPath) as! ImageCVC
        let imageURLString = self.arrImages[indexPath.row].attachment ?? ""
        cell.setData(imageURLString: imageURLString)
        return cell
    }
}
extension JobAssessmentTVC:UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.pushToSlideShow()
    }
}
extension JobAssessmentTVC:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let length = collectionView.frame.height
        let size = CGSize(width: length, height: length)
        return size
    }
}
