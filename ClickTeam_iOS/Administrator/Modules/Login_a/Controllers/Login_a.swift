//
//  Login_a.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 19/05/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import ObjectMapper

class Login_a: UIViewController {

    @IBOutlet weak var tfEmailAddress: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.tfEmailAddress.text = "superadmin@mothercare.com"
////        self.tfEmailAddress.text = "edge@yopmail.com"
//        self.tfPassword.text = "admin123"
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AppDelegate.shared.getCurrentWifiInfo()
    }
    
    @IBAction func onBtnLogin(_ sender: UIButton) {
        self.validate(sender: sender)
    }
    private func validate(sender:UIButton){
        let email = self.tfEmailAddress.text ?? ""
        let password = self.tfPassword.text ?? ""
        if !Validation.isValidEmail(email){
            Utility.main.showToast(message: Strings.INVALID_EMAIL.text)
            sender.shake()
            return
        }
        if !Validation.isValidPassword(password){
            Utility.main.showToast(message: Strings.INVALID_PWD.text)
            sender.shake()
            return
        }
        self.loginAdmin()
    }
}
//MARK:- Service
extension Login_a{
    private func loginAdmin(){
        let ip_address = Constants.ConnectedWifi
        let email = self.tfEmailAddress.text ?? ""
        let password = self.tfPassword.text ?? ""
        let device_type = "ios"
        let device_token = Constants.DeviceToken
        let is_admin = "true"
        let params:[String:Any] = ["ip_address":ip_address,
                                   "email":email,
                                   "password":password,
                                   "device_type":device_type,
                                   "device_token":device_token,
                                   "is_admin":is_admin]
        APIManager.sharedInstance.usersAPIManager.LoginAdmin(params: params, success: { (responseObject) in
            let response = responseObject as NSDictionary
            guard let userObject = response["user"] as? [String:Any] else {return}
            guard let user = Mapper<User>().map(JSON: userObject) else {return}
            AppStateManager.sharedInstance.loginUser(user: user)
        }) { (error) in
            print(error.localizedDescription)
        }
    }
}
