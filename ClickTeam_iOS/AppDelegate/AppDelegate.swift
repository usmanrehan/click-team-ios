//
//  AppDelegate.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 27/02/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SystemConfiguration.CaptiveNetwork
import UserNotifications
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    static let shared: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    let notificationCenter = UNUserNotificationCenter.current()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        // Override point for customization after application launch.
        self.getCurrentWifiInfo()
        IQKeyboardManager.shared.enable = true
        self.changeRootViewController()
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        Messaging.messaging().isAutoInitEnabled = true
        self.notificationCenter.delegate = self
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (_, error) in
            guard error == nil else{
                print(error!.localizedDescription)
                return
            }
        }
        
        application.registerForRemoteNotifications()
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
}
extension AppDelegate{
    public func changeRootViewController(){
//       if Global.APP_MANAGER.isUserLoggedIn(){
//            self.showHomeAdmin()
//        }
//        else{
//            self.showLoginAdmin()
//        }
        if Global.APP_MANAGER.isUserLoggedIn(){
            self.showHomeEmployee()
        }
        else{
            self.showLoginEmployee()
        }
    }
    private func showLoginEmployee(){
        let controller = AppStoryboard.Login.instance.instantiateViewController(withIdentifier: "Login")
        self.window?.rootViewController = controller
    }
    private func showHomeEmployee(){
        let storybaord = AppStoryboard.Home.instance
        let controller = storybaord.instantiateViewController(withIdentifier: "HomeEmployee")
        let navController = BaseNavigationController.init(rootViewController: controller)
        navController.navigationBar.isHidden = true
        self.window?.rootViewController = navController
    }
    private func showLoginAdmin(){
        let controller = AppStoryboard.Login_a.instance.instantiateViewController(withIdentifier: "Login_a")
        self.window?.rootViewController = controller
    }
    private func showHomeAdmin(){
        let storybaord = AppStoryboard.Home_a.instance
        let controller = storybaord.instantiateViewController(withIdentifier: "Home_a")
        let navController = BaseNavigationController.init(rootViewController: controller)
        navController.navigationBar.isHidden = true
        self.window?.rootViewController = navController
    }
}
//MARK:- Get wifi name
extension AppDelegate{
    func getCurrentWifiInfo(){
        if let interface = CNCopySupportedInterfaces() {
            for i in 0..<CFArrayGetCount(interface) {
                let interfaceName: UnsafeRawPointer = CFArrayGetValueAtIndex(interface, i)
                let rec = unsafeBitCast(interfaceName, to: AnyObject.self)
                if let unsafeInterfaceData = CNCopyCurrentNetworkInfo("\(rec)" as CFString), let interfaceData = unsafeInterfaceData as? [String : AnyObject] {
                    // connected wifi
                    if let connectedWifi = interfaceData["SSID"] as? String{
                        Constants.ConnectedWifi = connectedWifi
                    }
                    else{
                        Constants.ConnectedWifi = ""
                    }
                    //print("BSSID: \(interfaceData["BSSID"]), SSID: \(interfaceData["SSID"]), SSIDDATA: \(interfaceData["SSIDDATA"])")
                } else {
                    // not connected wifi
                }
            }
        }
    }
}
extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        Constants.DeviceToken = fcmToken
        print(Constants.DeviceToken)
        //        let fcmToken : [String: String] = ["Token": fcmToken]
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
        //        NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil, userInfo: fcmToken)
        
    }
    // [END refresh_token]
    
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
}
// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        print(userInfo)
        completionHandler([.alert, .badge, .sound])
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        print(userInfo)
        completionHandler()
    }
}
