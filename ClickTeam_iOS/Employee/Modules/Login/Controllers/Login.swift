//
//  Login.swift
//  ClickTeam_iOS
//
//  Created by Syed Hassan Shah on 28/02/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import ObjectMapper
import AVKit

class Login: UIViewController {

    @IBOutlet weak var tfEmailAddress: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    
    var imagePicker: UIImagePickerController!
    var profileImage: UIImage?
    var adminInfo = [AdminInfoModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppDelegate.shared.getCurrentWifiInfo()
//        self.tfEmailAddress.text = "junaid.quadri@hotmail.com"
//        self.tfPassword.text = "admin123"
        self.getAdminInfo()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
       // Utility.main.showAlert(message: Constants.ConnectedWifi, title: "Wifi Name", controller: self)
    }
    
    @IBAction func onLoginPressed(_ sender: Any) {
        self.validate(sender: sender as! UIButton)
    }
    @IBAction func onCallAdministrator(_ sender: Any) {
        let number = self.adminInfo.last?.value ?? ""
        Utility.main.makeCallTo(number: number)
    }
}
//MARK:- Helper Methods
extension Login{
    private func validate(sender:UIButton){
        let email = self.tfEmailAddress.text ?? ""
        let password = self.tfPassword.text ?? ""
        let image = self.profileImage
        if !Validation.isValidEmail(email){
            Utility.main.showToast(message: Strings.INVALID_EMAIL.text)
            sender.shake()
            return
        }
        if !Validation.isValidPassword(password){
            Utility.main.showToast(message: Strings.INVALID_PWD.text)
            sender.shake()
            return
        }
        if image == nil{
            self.uploadFromCamera()
            sender.shake()
            return
        }
        self.loginEmployee()
    }
    func checkCameraAccess(isAllowed: @escaping (Bool) -> Void) {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .denied:
            isAllowed(false)
        case .restricted:
            isAllowed(false)
        case .authorized:
            isAllowed(true)
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { isAllowed($0) }
        }
    }
}
//MARK:- Image picker
extension Login{
    func uploadFromCamera(){
        if let _ = self.profileImage {return}
        if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized {
            // Already Authorized
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.imagePicker =  UIImagePickerController()
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera
                self.imagePicker.cameraDevice = .front
                Utility.main.topViewController()?.present(self.imagePicker, animated: true, completion: nil)
            }
        }
        else{
            AVCaptureDevice.requestAccess(for: .video) { (isAllowed) in
                if isAllowed{
                    self.uploadFromCamera()
                }
                else{
                    Utility.main.goToSettings()
                }
            }
        }
    }
}
//MARK: - UIImagePickerControllerDelegate
extension Login: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            self.profileImage = pickedImage
        }
        Utility.main.topViewController()?.dismiss(animated: true, completion: nil)
    }
}
//MARK:- Services
extension Login{
    private func loginEmployee(){
        let ip_address = Constants.ConnectedWifi
        let email = self.tfEmailAddress.text ?? ""
        let password = self.tfPassword.text ?? ""
        let device_type = "ios"
        let device_token = Constants.DeviceToken
        let is_admin = "false"
        let image = self.profileImage ?? UIImage()
        let imageData = image.jpegData(compressionQuality: 0.1) ?? Data()
        
        let params:[String:Any] = ["ip_address":ip_address,
                                   "email":email,
                                   "password":password,
                                   "device_type":device_type,
                                   "device_token":device_token,
                                   "is_admin":is_admin,
                                   "image":imageData]
        print(params)
        APIManager.sharedInstance.usersAPIManager.LoginEmployee(params: params, success: { (responseObject) in
            let response = responseObject as NSDictionary
            guard let userObject = response["user"] as? [String:Any] else {return}
            guard let user = Mapper<User>().map(JSON: userObject) else {return}
            AppStateManager.sharedInstance.loginUser(user: user)
        }) { (error) in
            self.profileImage = nil
            print(error.localizedDescription)
        }
    }
    private func getAdminInfo(){
        let search = "name:admin_contact"
        let params:[String:Any] = ["search":search]
        APIManager.sharedInstance.usersAPIManager.GetAdminInfo(params: params, success: { (responseObject) in
            print(responseObject)
            guard let admin_info = responseObject as? [[String : Any]] else {return}
            self.adminInfo = Mapper<AdminInfoModel>().mapArray(JSONArray: admin_info)
        }) { (error) in
            print(error)
        }
    }
}
