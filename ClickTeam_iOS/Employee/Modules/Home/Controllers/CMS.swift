//
//  CMS.swift
//  ClickTeam_iOS
//
//  Created by Syed Hassan Shah on 04/03/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit

class CMS: BaseController {
    var selectionType = SelectionType.Training
    
    @IBOutlet weak var tvCMS: UITextView!
    @IBOutlet weak var imgCMSImage: UIImageView!
    
    var content = CMSModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setData()
        // Do any additional setup after loading the view.
    }
}
//MARK:- Helper Methods
extension CMS{
    private func setData(){
        switch selectionType.rawValue {
        case SelectionType.Training.rawValue:
            self.title = "Training"
        case SelectionType.Departments.rawValue:
            self.title = "Departments"
        case SelectionType.Policy.rawValue:
            self.title = "Policy"
        case SelectionType.AbsoluteRules.rawValue:
            self.title = "Absolute Rules"
        case SelectionType.Partners.rawValue:
            self.title = "Partners"
        default:
            break
        }
        self.tvCMS.text = self.content.content ?? ""
        self.tvCMS.contentOffset = .zero
    }
}

