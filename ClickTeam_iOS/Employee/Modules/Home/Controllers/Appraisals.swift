//
//  Appraisals.swift
//  ClickTeam_iOS
//
//  Created by Syed Hassan Shah on 05/03/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import ObjectMapper
import Cosmos

class Appraisals: BaseController {
    
    @IBOutlet weak var tfDate: UITextField!
    @IBOutlet weak var punchuality: CosmosView!
    @IBOutlet weak var customerService: CosmosView!
    @IBOutlet weak var respectForPolicy: CosmosView!
    @IBOutlet weak var initiationLeaderShip: CosmosView!
    @IBOutlet weak var teamWork: CosmosView!
    @IBOutlet weak var jobKnowledge: CosmosView!
    @IBOutlet weak var jobAttitude: CosmosView!
    @IBOutlet weak var other: CosmosView!
    
    var selectedDate = Date()
    var appraisals: AppraisalModel?
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Appraisals"
        self.tfDate.delegate = self
        self.setDate(date: self.selectedDate)
        self.setData()
        self.getEmployeeAppraisal()
        // Do any additional setup after loading the view.
    }
    @IBAction func onTfSelectDate(_ sender: UITextField) {
        self.getPickedDate(sender)
    }
}
//MARK:- Helper Methods
extension Appraisals{
    private func setDate(date:Date){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM yyyy"
        self.tfDate.text = dateFormatter.string(from: date)
        self.selectedDate = date
    }
    private func getSelectedDate()->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-yyyy"
        return dateFormatter.string(from: self.selectedDate)
    }
    private func setData(){
        if let data = self.appraisals{
            let puntuality_ = Double(data.punctuality)
            let customerService_ = Double(data.customerServices)
            let respectForPolicy_ = Double(data.respect)
            let initiationLeaderShip_ = Double(data.initiation)
            let teamWork_ = Double(data.teamwork)
            let jobAcknowledge_ = Double(data.jobKnowledge)
            let jobAttitude_ = Double(data.jobAttitude)
            let others_ = Double(data.other)
            
            self.punchuality.rating = puntuality_
            self.customerService.rating = customerService_
            self.respectForPolicy.rating = respectForPolicy_
            self.initiationLeaderShip.rating = initiationLeaderShip_
            self.teamWork.rating = teamWork_
            self.jobKnowledge.rating = jobAcknowledge_
            self.jobAttitude.rating = jobAttitude_
            self.other.rating = others_
        }
        else{
            self.punchuality.rating = 0.0
            self.customerService.rating = 0.0
            self.respectForPolicy.rating = 0.0
            self.initiationLeaderShip.rating = 0.0
            self.teamWork.rating = 0.0
            self.jobKnowledge.rating = 0.0
            self.jobAttitude.rating = 0.0
            self.other.rating = 0.0
        }
    }
}
//MARK:- Date picker
extension Appraisals{
    private func getPickedDate(_ sender: UITextField){
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = .date
        datePickerView.maximumDate = Date()
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: .valueChanged)
    }
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        self.setDate(date: sender.date)
    }
}
extension Appraisals:UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.getEmployeeAppraisal()
    }
}
extension Appraisals{
    private func getEmployeeAppraisal(){
        let user_id = AppStateManager.sharedInstance.loggedInUser.id
        let month = self.getSelectedDate()
        let search = "month:\(month)"
        let params:[String:Any] = ["search":search,"user_id":user_id]
        print(params)
        APIManager.sharedInstance.usersAPIManager.GetAppraisalReport(params: params, success: { (responseObject) in
            print(responseObject)
            guard let appraisal = responseObject as? [[String : Any]] else {return}
            let records = Mapper<AppraisalModel>().mapArray(JSONArray: appraisal)
            self.appraisals = records.last
            self.setData()
        }) { (error) in
            print(error)
        }
    }
}
