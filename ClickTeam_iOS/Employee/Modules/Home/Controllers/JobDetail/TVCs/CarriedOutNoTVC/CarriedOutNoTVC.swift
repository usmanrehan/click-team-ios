//
//  CarriedOutNoTVC.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 04/03/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit

class CarriedOutNoTVC: UITableViewCell {

    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var tvDescription: UITextView!
    @IBOutlet weak var btnSubmit: UIButton!
    
}
