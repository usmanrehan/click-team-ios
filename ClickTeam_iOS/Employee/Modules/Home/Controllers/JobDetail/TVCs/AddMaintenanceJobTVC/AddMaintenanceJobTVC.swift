//
//  AddMaintenanceJobTVC.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 16/05/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit

class AddMaintenanceJobTVC: UITableViewCell {

    @IBOutlet weak var tfJobTitle: UITextField!
    @IBOutlet weak var tfJobDescription: UITextField!
    @IBOutlet weak var tfDate: UITextField!
    var selectedDate = Date()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setDate(date: self.selectedDate)
    }

    @IBAction func onTfDatePicker(_ sender: UITextField) {
        self.getPickedDate(sender)
    }
    
}
//MARK:- Helper Methods
extension AddMaintenanceJobTVC{
    private func setDate(date:Date){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd,MMM yyyy"
        self.tfDate.text = dateFormatter.string(from: date)
        self.selectedDate = date
    }
    func getDate()->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd h:mm:ss"
        return dateFormatter.string(from: self.selectedDate)
    }
}
//MARK:- Date picker
extension AddMaintenanceJobTVC{
    private func getPickedDate(_ sender: UITextField){
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = .date
        datePickerView.maximumDate = Date()
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: .valueChanged)
    }
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        self.setDate(date: sender.date)
    }
}
