//
//  ImageCVC.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 15/05/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import SDWebImage

class ImageCVC: UICollectionViewCell {

    @IBOutlet weak var imgImage: UIImageView!
    
    func setData(image:UIImage){
        self.imgImage.image = image
    }
    func setData(imageURLString:String){
        if let imageURL = URL(string: imageURLString){
            self.imgImage.sd_setImage(with: imageURL, completed: nil)
        }
    }
}
