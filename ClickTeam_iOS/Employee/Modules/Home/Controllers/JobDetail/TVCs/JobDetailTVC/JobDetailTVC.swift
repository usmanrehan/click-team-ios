//
//  JobDetailTVC.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 04/03/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit

class JobDetailTVC: UITableViewCell {

    @IBOutlet weak var lblJobTitle: UILabel!
    @IBOutlet weak var lblJobDescription: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblDate: UILabel!
    
    var currentJob = JobModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.registerCVC()
    }
}
//MARK:- Helper Methods
extension JobDetailTVC{
    func setData(){
        let data = self.currentJob
        self.lblJobTitle.text = data.title ?? "-"
        self.lblJobDescription.text = data.detail ?? "-"
        self.lblStatus.text = "Inprogress"
        let date = data.startDate ?? "2019-04-05 12:04:00"//"yyyy-MM-dd hh:mm:ss"
        self.lblDate.text = Utility.stringDateFormatter(dateStr: date, dateFormat: "yyyy-MM-dd hh:mm:ss", formatteddate: "dd MMM yyyy hh:mm a")
    }
    private func registerCVC(){
        self.collectionView.register(UINib(nibName: "ImageCVC", bundle: nil), forCellWithReuseIdentifier: "ImageCVC")
    }
}
//MARK:- UICollectionViewDataSource
extension JobDetailTVC:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.currentJob.comments.first?.images.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCVC", for: indexPath) as! ImageCVC
        let image = self.currentJob.comments.first?.images[indexPath.item].attachment ?? ""
        cell.setData(imageURLString: image)
        return cell
    }
}
extension JobDetailTVC:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize(width: 70, height: 76)
        return size
    }
}
