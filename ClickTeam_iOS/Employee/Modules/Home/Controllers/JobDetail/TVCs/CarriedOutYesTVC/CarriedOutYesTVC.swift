//
//  CarriedOutYesTVC.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 04/03/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import MobileCoreServices

class CarriedOutYesTVC: UITableViewCell {

    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var tfEnterQuantity: UITextField!
    @IBOutlet weak var btnUploadImage: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnUploadFile: UIButton!
    @IBOutlet weak var viewLink: UIView!
    @IBOutlet weak var tfLink: UITextField!
    @IBOutlet weak var tvComment: UITextView!
    @IBOutlet weak var btnSubmit: UIButton!
    
    var imagePicker: UIImagePickerController!
    var arrImages = [UIImage]()
    var document: NSData?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.registerCVC()
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
    }
    
    @IBAction func onBtnUploadImages(_ sender: UIButton) {
        self.uploadImage()
    }
    @IBAction func onBtnUploadFile(_ sender: UIButton) {
        self.uploadFromiCloudDrive()
    }
    
//    func setData(jobDetailType:JobDetailType){
//        if jobDetailType == .addMaintenance{
//            self.viewLink.isHidden = true
//        }
//    }
}
extension CarriedOutYesTVC{
    private func registerCVC(){
        self.collectionView.register(UINib(nibName: "ImageCVC", bundle: nil), forCellWithReuseIdentifier: "ImageCVC")
    }
}
extension CarriedOutYesTVC:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrImages.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCVC", for: indexPath) as! ImageCVC
        cell.setData(image: self.arrImages[indexPath.item])
        return cell
    }
}
extension CarriedOutYesTVC:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let length = collectionView.frame.height
        let size = CGSize(width: length, height: length)
        return size
    }
}
//MARK:- Upload Document
extension CarriedOutYesTVC{
    private func uploadFromiCloudDrive(){
        let importMenu = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        Utility.main.topViewController()?.present(importMenu, animated: true, completion: nil)
    }
}
//MARK: - Document Picker
extension CarriedOutYesTVC: UIDocumentPickerDelegate,UIDocumentMenuDelegate{
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        Utility.main.topViewController()?.present(documentPicker, animated: true, completion: nil)
    }
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let fileManager = FileManager.default
        print(fileManager.fileExists(atPath: url.path))
        let data = NSData(contentsOfFile: url.path)
        guard let doc = data else {
            Utility.main.showAlert(message: "Error", title: "Document format not supported!", controller: Utility.main.topViewController()!)
            return
        }
        self.document = doc
        self.btnUploadFile.setTitle("Document attached", for: .normal)
        Utility.main.showAlert(message: "Success", title: "Document attached successfully.", controller: Utility.main.topViewController()!)
    }
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("we cancelled")
        Utility.main.topViewController()?.dismiss(animated: true, completion: nil)
    }
}
//MARK:- Image picker
extension CarriedOutYesTVC{
    func uploadFromCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            self.imagePicker =  UIImagePickerController()
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .camera
            Utility.main.topViewController()?.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    func uploadFromGallery(){
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        Utility.main.topViewController()?.present(picker, animated: true, completion: nil)
    }
    func uploadImage(){
        let alert = UIAlertController(title: "Upload Photo" , message: "How do you want to set your photo?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (UIAlertAction) in
            self.uploadFromCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (UIAlertAction) in
            self.uploadFromGallery()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        Utility.main.topViewController()?.present(alert, animated: true, completion: nil)
    }
}
//MARK: - UIImagePickerControllerDelegate
extension CarriedOutYesTVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            self.arrImages.append(pickedImage)
            self.collectionView.reloadData()
        }
        Utility.main.topViewController()?.dismiss(animated: true, completion: nil)
    }
}
