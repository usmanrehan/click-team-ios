//
//  JobDetail.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 04/03/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit

enum CarriedOutType {
    case InitialView
    case CarriedOutYes
    case CarriedOutNo
}

enum JobDetailType{
    case detail
    case addMaintenance
}

class JobDetail: BaseController {

    @IBOutlet weak var tableView: UITableView!
    
    var carriedOutType = CarriedOutType.InitialView
    var jobDetailType = JobDetailType.detail
    var currentJob = JobModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Job Details"
        self.registerCells()
        // Do any additional setup after loading the view.
    }
}
//MARK:- Helper Methods
extension JobDetail{
    private func registerCells(){
        self.tableView.register(UINib(nibName: "JobDetailTVC", bundle: nil), forCellReuseIdentifier: "JobDetailTVC")
        self.tableView.register(UINib(nibName: "AddMaintenanceJobTVC", bundle: nil), forCellReuseIdentifier: "AddMaintenanceJobTVC")
        self.tableView.register(UINib(nibName: "CarriedOutTVC", bundle: nil), forCellReuseIdentifier: "CarriedOutTVC")
        self.tableView.register(UINib(nibName: "CarriedOutYesTVC", bundle: nil), forCellReuseIdentifier: "CarriedOutYesTVC")
        self.tableView.register(UINib(nibName: "CarriedOutNoTVC", bundle: nil), forCellReuseIdentifier: "CarriedOutNoTVC")
    }
    private func reloadUI(){
        self.tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .automatic)
    }
    private func validateCarriedOutYes(sender:UIButton)->Bool{
        let indexJobInfo = IndexPath(row: 0, section: 0)
        if let cell = tableView.cellForRow(at: indexJobInfo) as? AddMaintenanceJobTVC{
            let title = cell.tfJobTitle.text ?? ""
            let description = cell.tfJobDescription.text ?? ""
            if !Validation.validateStringLength(title){
                Utility.main.showToast(message: "Job title is required")
                sender.shake()
                return false
            }
            if !Validation.validateStringLength(description){
                Utility.main.showToast(message: "Job description is required")
                sender.shake()
                return false
            }
        }
        let indexJobCompleteInfo = IndexPath(row: 1, section: 0)
        if let cell = tableView.cellForRow(at: indexJobCompleteInfo) as? CarriedOutYesTVC{
            let quantity = cell.tfEnterQuantity.text ?? ""
            let comment = cell.tvComment.text ?? ""
            if !Validation.validateStringLength(quantity){
                Utility.main.showToast(message: "Please enter quantity")
                sender.shake()
                return false
            }
            if !Validation.validateStringLength(comment){
                Utility.main.showToast(message: "Please add comment")
                sender.shake()
                return false
            }
        }
        return true
    }
    private func validateCarriedOutYesCurrentJob(sender:UIButton)->Bool{
        let indexJobCompleteInfo = IndexPath(row: 1, section: 0)
        if let cell = tableView.cellForRow(at: indexJobCompleteInfo) as? CarriedOutYesTVC{
            let quantity = cell.tfEnterQuantity.text ?? ""
            let comment = cell.tvComment.text ?? ""
            if !Validation.validateStringLength(quantity){
                Utility.main.showToast(message: "Please enter quantity")
                sender.shake()
                return false
            }
            if !Validation.validateStringLength(comment){
                Utility.main.showToast(message: "Please add comment")
                sender.shake()
                return false
            }
        }
        return true
    }
}
extension JobDetail:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            switch self.jobDetailType{
            case .detail:
                let cell = tableView.dequeueReusableCell(withIdentifier: "JobDetailTVC", for: indexPath) as! JobDetailTVC
                cell.currentJob = self.currentJob
                cell.setData()
                return cell
            case .addMaintenance:
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddMaintenanceJobTVC", for: indexPath) as! AddMaintenanceJobTVC
                return cell
            }
        case 1:
            switch self.carriedOutType{
            case .InitialView:
                let cell = tableView.dequeueReusableCell(withIdentifier: "CarriedOutTVC", for: indexPath) as! CarriedOutTVC
                cell.btnYes.addTarget(self, action: #selector(self.onBtnYesCarriedOut(sender:)), for: .touchUpInside)
                cell.btnNo.addTarget(self, action: #selector(self.onBtnNoCarriedOut(sender:)), for: .touchUpInside)
                return cell
            case .CarriedOutYes:
                let cell = tableView.dequeueReusableCell(withIdentifier: "CarriedOutYesTVC", for: indexPath) as! CarriedOutYesTVC
                cell.btnClose.addTarget(self, action: #selector(self.onBtnCloseCarriedOutYes(sender:)), for: .touchUpInside)
                cell.btnSubmit.addTarget(self, action: #selector(self.onBtnSubmitCarriedOutYes(sender:)), for: .touchUpInside)
                if self.jobDetailType == .addMaintenance{
                    cell.btnClose.isHidden = true
                }
                else{
                    cell.btnClose.isHidden = false
                }
                return cell
            case .CarriedOutNo:
                let cell = tableView.dequeueReusableCell(withIdentifier: "CarriedOutNoTVC", for: indexPath) as! CarriedOutNoTVC
                cell.btnClose.addTarget(self, action: #selector(self.onBtnCloseCarriedOutNo(sender:)), for: .touchUpInside)
                cell.btnSubmit.addTarget(self, action: #selector(self.onBtnSubmitCarriedOutNo(sender:)), for: .touchUpInside)
                return cell
            }
        default:
            return UITableViewCell()
        }
        
    }
}
//MARK:- Selector
extension JobDetail{
    //CarriedOutTVC
    @objc func onBtnYesCarriedOut(sender:UIButton){
        self.carriedOutType = .CarriedOutYes
        self.reloadUI()
    }
    @objc func onBtnNoCarriedOut(sender:UIButton){
        self.carriedOutType = .CarriedOutNo
        self.reloadUI()
    }
    //CarriedOutNoTVC
    @objc func onBtnCloseCarriedOutNo(sender:UIButton){
        self.carriedOutType = .InitialView
        self.reloadUI()
    }
    @objc func onBtnSubmitCarriedOutNo(sender:UIButton){
        Utility.main.showAlert(message: Strings.ASK_FOR_COMPLETE_JOB.text, title: Strings.JOB_DONE.text, controller: self) { (yes, no) in
            if yes != nil{
                self.postCarriedOutNo()
            }
        }
    }
    //CarriedOutYesTVC
    @objc func onBtnCloseCarriedOutYes(sender:UIButton){
        self.carriedOutType = .InitialView
        self.reloadUI()
    }
    @objc func onBtnSubmitCarriedOutYes(sender:UIButton){
        switch self.jobDetailType{
        case .detail:
            if !self.validateCarriedOutYesCurrentJob(sender: sender){return}
            else{
                Utility.main.showAlert(message: Strings.ASK_FOR_COMPLETE_JOB.text, title: Strings.JOB_DONE.text, controller: self) { (yes, no) in
                    if yes != nil{
                        self.postCarriedOutYes()
                    }
                }
            }
        case .addMaintenance:
            if !self.validateCarriedOutYes(sender: sender){return}
            else{
                Utility.main.showAlert(message: Strings.ASK_FOR_COMPLETE_JOB.text, title: Strings.JOB_DONE.text, controller: self) { (yes, no) in
                    if yes != nil{
                        self.addNewMaintenaceAndMarkComplete()
                    }
                }
            }
        }
    }
}
//MARK:- Services
extension JobDetail{
    private func getCarriedOutNo()->[String:Any]{
        let task_id = self.currentJob.id ?? "0"
        let user_id = AppStateManager.sharedInstance.loggedInUser.id
        let carried_out = "0"
        var comment = ""
        let index = IndexPath(row: 1, section: 0)
        if let cell = tableView.cellForRow(at: index) as? CarriedOutNoTVC{
            comment = cell.tvDescription.text ?? ""
        }
        return ["task_id":task_id,
                "user_id":user_id,
                "carried_out":carried_out,
                "comment":comment]
    }
    private func postCarriedOutNo(){
        let params = self.getCarriedOutNo()
        APIManager.sharedInstance.usersAPIManager.PostCarriedOutYesNo(params: params, success: { (responseObject) in
            self.navigationController?.popViewController(animated: true)
        }) { (error) in
            print(error)
        }
    }
    
    private func getCarriedOutYes()->[String:Any]{
        var params:[String:Any] = [:]
        
        let task_id = self.currentJob.id ?? "0"
        let user_id = AppStateManager.sharedInstance.loggedInUser.id
        let carried_out = "1"
        let status = "40"
        
        params["task_id"] = task_id
        params["user_id"] = user_id
        params["carried_out"] = carried_out
        params["status"] = status
        
        var quantity = ""
        var link = ""
        var comment = ""
        
        let index = IndexPath(row: 1, section: 0)
        if let cell = tableView.cellForRow(at: index) as? CarriedOutYesTVC{
            quantity = cell.tfEnterQuantity.text ?? ""
            link = cell.tfLink.text ?? ""
            comment = cell.tvComment.text ?? ""
            
            params["quantity"] = quantity
            params["link"] = link
            params["comment"] = comment
            
            for (index,image) in cell.arrImages.enumerated(){
                let imageData = image.jpegData(compressionQuality: 0.1) ?? Data()
                //UIImageJPEGRepresentation(image, 0.1) ?? Data()
                params["images[\(index)]"] = imageData
            }
            if let file = cell.document as Data?{
                params["file"] = file
            }
        }
        return params
    }
    private func postCarriedOutYes(){
        let params = self.getCarriedOutYes()
        print(params)
        APIManager.sharedInstance.usersAPIManager.PostCarriedOutYesNo(params: params, success: { (responseObject) in
            Utility.main.showAlert(message: "Maintenance job completed successfully", title: "Job completed", controller: self, usingCompletionHandler: {
                self.navigationController?.popViewController(animated: true)
            })
        }) { (error) in
            print(error)
        }
    }
    
    private func getNewMaintenaceAndMarkComplete()->[String:Any]{
        var params:[String:Any] = [:]
        
        let parent_task_id = "2"
        params["parent_task_id"] = parent_task_id
        
        let user_id = AppStateManager.sharedInstance.loggedInUser.id
        params["user_id"] = user_id
        
        let is_already_assigned = "1"
        params["is_already_assigned"] = is_already_assigned
        
        let carried_out = "1"
        params["carried_out"] = carried_out
        
        let priority = "10"
        params["priority"] = priority
        
        let status = "40"
        params["status"] = status
        
        let created_by = AppStateManager.sharedInstance.loggedInUser.name ?? ""
        params["created_by"] = created_by
        
        let indexJobInfo = IndexPath(row: 0, section: 0)
        if let cell = tableView.cellForRow(at: indexJobInfo) as? AddMaintenanceJobTVC{
            let title = cell.tfJobTitle.text ?? ""
            let detail = cell.tfJobDescription.text ?? ""
            let start_date = cell.getDate()
            let end_date = cell.getDate()
            
            params["title"] = title
            params["detail"] = detail
            params["start_date"] = start_date
            params["end_date"] = end_date
        }
        
        let indexJobCompleteInfo = IndexPath(row: 1, section: 0)
        if let cell = tableView.cellForRow(at: indexJobCompleteInfo) as? CarriedOutYesTVC{
            let quantity = cell.tfEnterQuantity.text ?? ""
            let link = cell.tfLink.text ?? ""
            let comment = cell.tvComment.text ?? ""
            
            params["quantity"] = quantity
            params["link"] = link
            params["comment"] = comment
            
            for (index,image) in cell.arrImages.enumerated(){
                let imageData = image.jpegData(compressionQuality: 0.1) ?? Data()
                params["images[\(index)]"] = imageData
            }
            if let file = cell.document as Data?{
                params["file"] = file
            }
        }
        return params
    }
    private func addNewMaintenaceAndMarkComplete(){
        let params = self.getNewMaintenaceAndMarkComplete()
        print(params)
        APIManager.sharedInstance.usersAPIManager.PostTaskwithCatComment(params: params, success: { (responseObject) in
            Utility.main.showAlert(message: "Maintenance job completed successfully", title: "Job competed", controller: self, usingCompletionHandler: {
                self.navigationController?.popViewController(animated: true)
            })
        }) { (error) in
            print(error)
        }
    }
}
