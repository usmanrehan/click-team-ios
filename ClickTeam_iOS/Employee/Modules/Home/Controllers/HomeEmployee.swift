//
//  Home.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 04/03/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import ObjectMapper

class HomeEmployee: UIViewController {

    @IBOutlet weak var imgProfile: RoundedImage!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    var adminInfo = [AdminInfoModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setData()
        self.getAdminInfo()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnLogout(_ sender: UIButton) {
        AppStateManager.sharedInstance.logoutUser()
    }
    @IBAction func onBtnNotification(_ sender: UIButton) {
        self.pushToNotifications()
    }
    @IBAction func onBtnAttendance(_ sender: UIButton) {
        self.pushToAttendance()
    }
    @IBAction func onBtnDuties(_ sender: UIButton) {
        self.pushToDuties()
    }
    @IBAction func onBtnEarningReport(_ sender: UIButton) {
        self.pushToEarningReport()
    }
    @IBAction func onBtnMaintanenceTask(_ sender: UIButton) {
        self.pushToMaintanenceTask()
    }
    @IBAction func onBtnMessage(_ sender: UIButton) {
        self.pushToMessage()
    }
    @IBAction func onBtnAppraisalReport(_ sender: UIButton) {
        self.pushToAppraisalReport()
    }
    @IBAction func onBtnComplaints(_ sender: UIButton) {
        self.pushToComplains()
    }
    @IBAction func onBtnCompany(_ sender: UIButton) {
        self.pushToCompany()
    }
    @IBAction func onBtnCallAdmin(_ sender: Any) {
        let number = self.adminInfo.last?.value ?? ""
        Utility.main.makeCallTo(number: number)
    }
}
extension HomeEmployee{
    private func pushToNotifications(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Notifications")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    private func pushToAttendance(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Attendance")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    private func pushToDuties(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Duties")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    private func pushToEarningReport(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Earning") as! Earning
        controller.isEmployee = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    private func pushToMaintanenceTask(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Maintenance")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    private func pushToMessage(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Messages")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    private func pushToComplains(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Messages") as! Messages
        controller.messageType = .complains
        self.navigationController?.pushViewController(controller, animated: true)
    }
    private func pushToAppraisalReport(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Appraisals")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    private func pushToCompany(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "Company")
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
//MARK:- Helper Methods
extension HomeEmployee{
    private func setData(){
        let user = AppStateManager.sharedInstance.loggedInUser
        self.lblUserName.text = user?.name ?? "-"
        self.lblEmail.text = user?.email ?? "-"
        let imageURLString = user?.details?.image ?? ""
        if let imageURL = URL(string: imageURLString){
            self.imgProfile.sd_setImage(with: imageURL, completed: nil)
        }
    }
}
//MARK:- Helper Methods
extension HomeEmployee{
    private func getAdminInfo(){
        let search = "name:admin_contact"
        let params:[String:Any] = ["search":search]
        APIManager.sharedInstance.usersAPIManager.GetAdminInfo(params: params, success: { (responseObject) in
            print(responseObject)
            guard let admin_info = responseObject as? [[String : Any]] else {return}
            self.adminInfo = Mapper<AdminInfoModel>().mapArray(JSONArray: admin_info)
        }) { (error) in
            print(error)
        }
    }
}
