//
//  Earning.swift
//  ClickTeam_iOS
//
//  Created by Syed Hassan Shah on 04/03/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import ObjectMapper

class Earning: BaseController {
    
    @IBOutlet weak var tfDate: UITextField!
    @IBOutlet weak var lblBasicSalary: UILabel!
    @IBOutlet weak var lblHoursWorked: UILabel!
    @IBOutlet weak var lblCashValue: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblEarnedPoints: UILabel!
    @IBOutlet weak var lblCash: UILabel!
    @IBOutlet weak var lblEarnedRatings: UILabel!
    @IBOutlet weak var lblConformanceCash: UILabel!
    @IBOutlet weak var lblTotalHours: UILabel!
    @IBOutlet weak var lblTotalLates: UILabel!
    @IBOutlet weak var lblIncomeTax: UILabel!
    @IBOutlet weak var lblSSNIT: UILabel!
    @IBOutlet weak var lblLossCaused: UILabel!
    @IBOutlet weak var lblOtherDeductions: UILabel!
    @IBOutlet weak var lblEarning: UILabel!
    @IBOutlet weak var lblTotalDeductions: UILabel!
    @IBOutlet weak var lblTotalEarning: UILabel!
    
    var user: EmployeeRecord?
    var selectedDate = Date()
    var earnings: EarningModel?
    var isEmployee = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Earning Report"
        self.tfDate.delegate = self
        self.setDate(date: selectedDate)
        self.getEmployeeReport()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onTfSelectDate(_ sender: UITextField) {
        self.getPickedDate(sender)
    }
}
//MARK:- Helper Methods
extension Earning{
    private func setDate(date:Date){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM yyyy"
        self.tfDate.text = dateFormatter.string(from: date)
        self.selectedDate = date
    }
    private func getSelectedDate()->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM"
        return dateFormatter.string(from: self.selectedDate)
    }
    private func setData(){
        if let data = self.earnings{
            self.lblBasicSalary.text = data.salary ?? "0"
            self.lblHoursWorked.text = data.hoursWorked ?? "0"
            self.lblCashValue.text = "\(data.cashValue)"
            self.lblTotal.text = data.totals ?? "0"
            self.lblEarnedPoints.text = "\(data.performanceEarned)"
            self.lblCash.text = "\(data.performanceCash)"
            self.lblEarnedRatings.text = "\(data.conformanceEarned)"
            self.lblConformanceCash.text = "\(data.confromanceCash)"
            self.lblTotalHours.text = data.hoursWorked ?? "0"
            self.lblTotalLates.text = "\(data.lates)"
            self.lblIncomeTax.text = data.incomeTax ?? "0"
            self.lblSSNIT.text = "\(data.ssnit)"
            self.lblLossCaused.text = "\(data.lossCauseCompany)"
            self.lblOtherDeductions.text = "\(data.other)"
            self.lblEarning.text = data.subtotals ?? "0"
            let subTotal = Double(data.subtotals ?? "0") ?? 0.0
            let total = Double(data.totals ?? "0") ?? 0.0
            let deduction = total - subTotal
            self.lblTotalDeductions.text = "\(deduction)"
            self.lblTotalEarning.text = data.totals ?? "0"
        }
        else{
            let pending = "Pending"
            self.lblBasicSalary.text = pending
            self.lblHoursWorked.text = pending
            self.lblCashValue.text = pending
            self.lblTotal.text = pending
            self.lblEarnedPoints.text = pending
            self.lblCash.text = pending
            self.lblEarnedRatings.text = pending
            self.lblConformanceCash.text = pending
            self.lblTotalHours.text = pending
            self.lblTotalLates.text = pending
            self.lblIncomeTax.text = pending
            self.lblSSNIT.text = pending
            self.lblLossCaused.text = pending
            self.lblOtherDeductions.text = pending
            self.lblEarning.text = pending
            self.lblTotalDeductions.text = pending
            self.lblTotalEarning.text = pending
        }
    }
}
//MARK:- Date picker
extension Earning{
    private func getPickedDate(_ sender: UITextField){
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = .date
        datePickerView.maximumDate = Date()
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: .valueChanged)
    }
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        self.setDate(date: sender.date)
    }
}
//MARK:- UITextFieldDelegate
extension Earning:UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.getEmployeeReport()
    }
}
//MARK:- Service
extension Earning{
    private func getEmployeeReport(){
        var user_id = 0
        if !self.isEmployee{
            user_id = self.user?.id ?? 0
        }
        else{
            let employee = AppStateManager.sharedInstance.loggedInUser
            user_id = employee?.id ?? 0
        }
        let month = self.getSelectedDate()
        let params:[String:Any] = ["search":"month:\(month)","user_id":"\(user_id)"]
        print(params)
        APIManager.sharedInstance.usersAPIManager.GetEarningReport(params: params, success: { (responseObject) in
            print(responseObject)
            guard let earnings = responseObject as? [[String : Any]] else {return}
            let records = Mapper<EarningModel>().mapArray(JSONArray: earnings)
            self.earnings = records.first
            self.setData()
        }) { (error) in
            print(error)
        }
    }
}
