//
//  Attendance.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 07/03/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import PieCharts
import ObjectMapper

class Attendance: BaseController {
    
    @IBOutlet weak var tfDate: UITextField!
    @IBOutlet weak var lblScheduleWorkHours: UILabel!
    @IBOutlet weak var lblWorkedHours: UILabel!
    @IBOutlet weak var lblNumberOfLates: UILabel!
    @IBOutlet weak var lblAbsents: UILabel!
    @IBOutlet weak var lblOvertime: UILabel!
    @IBOutlet weak var sliderWorkingHours: PieChart!
    @IBOutlet weak var sliderOverTime: PieChart!
    @IBOutlet weak var sliderLates: PieChart!
    @IBOutlet weak var sliderAbsents: PieChart!
    @IBOutlet weak var lblWorkingHours_: UILabel!
    @IBOutlet weak var lblOverTime_: UILabel!
    @IBOutlet weak var lblLates_: UILabel!
    @IBOutlet weak var lblAbsents_: UILabel!
    
    
    var selectedDate = Date()
    var attendance = AttendanceModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Attendance"
        self.tfDate.delegate = self
        self.setDate(date: Date())
        self.getAttendence()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onTfSelectDate(_ sender: UITextField) {
        self.getPickedDate(sender)
    }
}
//MARK:- Helper Methods
extension Attendance{
    private func setDate(date:Date){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM yyyy"
        self.tfDate.text = dateFormatter.string(from: date)
        self.selectedDate = date
    }
    private func getSelectedDate()->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM"
        return dateFormatter.string(from: self.selectedDate)
    }
}
//MARK:- Set PieCharts
extension Attendance{
    private func setAllSliders(){
        self.setSliderWorkingHours()
        self.setSliderOverTime()
        self.setSliderLates()
        self.setSliderAbsents()
    }
    private func setSliderWorkingHours(){
        let overAll = Double(self.attendance.workingHours ?? "0") ?? 0.0
        let acheived = Double(self.attendance.workedHours ?? "0") ?? 0.0
        
        self.sliderWorkingHours.innerRadius = self.sliderWorkingHours.frame.height * 0.35
        self.sliderWorkingHours.outerRadius = self.sliderWorkingHours.frame.height * 0.425
        self.sliderWorkingHours.models = [
            PieSliceModel(value: overAll - acheived, color: .darkGray),
            PieSliceModel(value: acheived, color: Global.APP_COLOR)
        ]
    }
    private func setSliderOverTime(){
        let overAll = Double(self.attendance.workingHours ?? "0") ?? 0.0
        let acheived = Double(self.attendance.overtime)
        
        self.sliderOverTime.innerRadius = self.sliderOverTime.frame.height * 0.35
        self.sliderOverTime.outerRadius = self.sliderOverTime.frame.height * 0.425
        self.sliderOverTime.models = [
            PieSliceModel(value: overAll - acheived, color: .darkGray),
            PieSliceModel(value: acheived, color: Global.APP_COLOR)
        ]
    }
    private func setSliderLates(){
        let overAll = Double(30)
        let acheived = Double(self.attendance.lates)

        self.sliderLates.innerRadius = self.sliderLates.frame.height * 0.35
        self.sliderLates.outerRadius = self.sliderLates.frame.height * 0.425
        self.sliderLates.models = [
            PieSliceModel(value: overAll - acheived, color: .darkGray),
            PieSliceModel(value: acheived, color: Global.APP_COLOR)
        ]
    }
    private func setSliderAbsents(){
        let overAll = Double(30)
        let acheived = Double(self.attendance.lates)
        
        self.sliderAbsents.innerRadius = self.sliderAbsents.frame.height * 0.35
        self.sliderAbsents.outerRadius = self.sliderAbsents.frame.height * 0.425
        self.sliderAbsents.models = [
            PieSliceModel(value: overAll - acheived, color: .darkGray),
            PieSliceModel(value: acheived, color: Global.APP_COLOR)
        ]
    }
    private func setData(){
        let data = self.attendance
        self.lblScheduleWorkHours.text = "\(data.workingHours ?? "0") hours"
        self.lblWorkedHours.text = "\(data.workedHours ?? "0") hours"
        self.lblNumberOfLates.text = "\(data.lates)"
        self.lblAbsents.text = "\(data.lates)"
        self.lblOvertime.text = "\(data.overtime) hours"
        
        let overAllWorkingHours = Double(self.attendance.workingHours ?? "0") ?? 0.0
        let acheivedWorkingHours = Double(self.attendance.workedHours ?? "0") ?? 0.0
        self.lblWorkingHours_.text = "\(acheivedWorkingHours)/\(overAllWorkingHours)"
        
        let overAllOverTime = Double(self.attendance.workingHours ?? "0") ?? 0.0
        let acheivedOverTime = Double(self.attendance.overtime)
        self.lblOverTime_.text = "\(acheivedOverTime)/\(overAllOverTime)"
        
        let overAllLates = Double(30)
        let acheivedLates = Double(self.attendance.lates)
        self.lblLates_.text = "\(acheivedLates)/\(overAllLates)"
        
        let overAllAbsents = Double(30)
        let acheivedAbsents = Double(self.attendance.lates)
        self.lblAbsents_.text = "\(acheivedAbsents)/\(overAllAbsents)"
    }
}
//MARK:- Date picker
extension Attendance{
    private func getPickedDate(_ sender: UITextField){
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = .date
        datePickerView.maximumDate = Date()
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: .valueChanged)
    }
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        self.setDate(date: sender.date)
    }
}
//MARK:-
extension Attendance:UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.getAttendence()
    }
}
extension Attendance{
    private func getAttendence(){
        let month = self.getSelectedDate()
        let params:[String:Any] = ["month":month]
        APIManager.sharedInstance.usersAPIManager.GetAttendances(params: params, success: { (responseObject) in
            print(responseObject)
            self.attendance = Mapper<AttendanceModel>().map(JSON: responseObject) ?? AttendanceModel()
            self.setAllSliders()
            self.setData()
        }) { (error) in
            print(error)
        }
    }
}
