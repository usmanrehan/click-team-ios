//
//  ComplainTVC.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 22/06/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import Cosmos

class ComplainTVC: UITableViewCell {
    
    @IBOutlet weak var tfTitle: UITextField!
    @IBOutlet weak var tfDate: UITextField!
    @IBOutlet weak var viewRating: CosmosView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tvComment: UITextView!
    var complain = ComplainModel()
    var arrImagesURLs = [String]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.registerCVC()
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        // Initialization code
    }
    private func registerCVC(){
        self.collectionView.register(UINib(nibName: "ImageCVC", bundle: nil), forCellWithReuseIdentifier: "ImageCVC")
    }
    func setData(data:ComplainModel){
        self.complain = data
        self.tfTitle.text = data.complain ?? "-"
        let date = data.createdAt ?? "2019-04-05 12:04:00"//"yyyy-MM-dd hh:mm:ss"
        self.tfDate.text = Utility.stringDateFormatter(dateStr: date, dateFormat: "yyyy-MM-dd hh:mm:ss", formatteddate: "dd MMM yyyy hh:mm a")
        let rate = Double(data.complainRate ?? "0") ?? 0.0
        self.viewRating.rating = rate
        self.tvComment.text = data.complainComment ?? "-"
        self.arrImagesURLs = (data.images ?? "").replacingOccurrences(of: " ", with: "").components(separatedBy: ",")
        self.collectionView.reloadData()
    }
}
extension ComplainTVC{
    private func pushToSlideShow(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "SlideShow") as! SlideShow
        controller.complain = self.complain
        controller.arrImages = self.arrImagesURLs
        Utility.main.topViewController()?.navigationController?.pushViewController(controller, animated: true)
    }
}
//MARK:- UICollectionViewDataSource
extension ComplainTVC:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrImagesURLs.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCVC", for: indexPath) as! ImageCVC
        cell.setData(imageURLString: self.arrImagesURLs[indexPath.item])
        return cell
    }
}
extension ComplainTVC:UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.pushToSlideShow()
    }
}
extension ComplainTVC:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let length = collectionView.frame.height
        let size = CGSize(width: length, height: length)
        return size
    }
}
