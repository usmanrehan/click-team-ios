//
//  MessagesTVC.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 22/06/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit

class MessagesTVC: UITableViewCell {

    @IBOutlet weak var lblSender: UILabel!
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    func setData(data:MessageModel){
        self.lblSender.text = data.userFrom ?? "-"
        self.lblSubject.text = data.subject ?? "-"
        self.lblMessage.text = data.message ?? "-"
        let dateStr = data.createdAt ?? "2019-04-05 12:04:00"
        let date = Utility.stringDateFormatter(dateStr: dateStr, dateFormat: "YYYY-MM-dd HH:mm:ss", formatteddate: "h:mm a, dd-MMM-yyyy")
        self.lblDate.text = "Message On: \(date)"
    }
}
//2019-06-30 17:18:29
