//
//  Messages.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 22/06/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import ObjectMapper

enum MessageType{
    case messages
    case complains
}

class Messages: BaseController {

    @IBOutlet weak var tableView: UITableView!
    
    var arrMessages = [MessageModel]()
    var arrComplains = [ComplainModel]()
    var messageType = MessageType.messages
    
    override func viewDidLoad() {
        super.viewDidLoad()
        switch self.messageType{
        case .messages:
            self.title = "Messages"
            self.getMessages()
        case .complains:
            self.title = "Complains"
            self.getComplains()
        }
        // Do any additional setup after loading the view.
    }
}
extension Messages:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.messageType{
        case .messages:
            return self.arrMessages.count
        case .complains:
            return self.arrComplains.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch self.messageType{
        case .messages:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MessagesTVC", for: indexPath) as! MessagesTVC
            cell.setData(data: self.arrMessages[indexPath.row])
            return cell
        case .complains:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ComplainTVC", for: indexPath) as! ComplainTVC
            cell.setData(data: self.arrComplains[indexPath.row])
            return cell
        }
    }
}
//MARK:- Service
extension Messages{
    private func getMessages(){
        let orderBy = "id"
        let sortedBy = "desc"
        let params:[String:Any] = ["orderBy":orderBy,
                                   "sortedBy":sortedBy]
        APIManager.sharedInstance.usersAPIManager.GetMessages(params: params, success: { (responseObject) in
            guard let messages = responseObject as? [[String : Any]] else {return}
            self.arrMessages = Mapper<MessageModel>().mapArray(JSONArray: messages)
            self.tableView.reloadData()
            if self.arrMessages.isEmpty{
                Utility.main.showToast(message: "No messages available")
            }
        }) { (error) in
            print(error)
        }
    }
    private func getComplains(){
        let user_id = AppStateManager.sharedInstance.loggedInUser.id
        let search = "user_id:\(user_id)"
        let params:[String:Any] = ["search":search]
        APIManager.sharedInstance.usersAPIManager.GetComplains(params: params, success: { (responseObject) in
            guard let complains = responseObject as? [[String : Any]] else {return}
            self.arrComplains = Mapper<ComplainModel>().mapArray(JSONArray: complains)
            self.tableView.reloadData()
            if self.arrComplains.isEmpty{
                Utility.main.showToast(message: "No complains available")
            }
        }) { (error) in
            print(error)
        }
    }
}
