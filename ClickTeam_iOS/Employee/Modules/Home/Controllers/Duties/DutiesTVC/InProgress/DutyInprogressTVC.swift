//
//  DutiesTVC.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 04/03/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit

class DutyInprogressTVC: UITableViewCell {

    @IBOutlet weak var lblJobTitle: UILabel!
    @IBOutlet weak var lblJobDescription: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    func setData(data:JobModel){
        self.lblJobTitle.text = data.title ?? "-"
        self.lblJobDescription.text = data.detail ?? "-"
        let date = data.startDate ?? "2019-04-05 12:04:00"//"yyyy-MM-dd hh:mm:ss"
        self.lblDate.text = Utility.stringDateFormatter(dateStr: date, dateFormat: "yyyy-MM-dd hh:mm:ss", formatteddate: "dd MMM yyyy hh:mm a")
    }
    
}
