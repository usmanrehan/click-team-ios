//
//  DutiesTVC.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 04/03/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import Cosmos

class DutyHistoryTVC: UITableViewCell {

    @IBOutlet weak var lblJobTitle: UILabel!
    @IBOutlet weak var lblJobDescription: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblSupervisorComment: UILabel!
    @IBOutlet weak var viewRating: CosmosView!
    @IBOutlet weak var lblDate: UILabel!
    
    func setData(data:JobModel){
        self.lblJobTitle.text = data.title ?? ""
        self.lblJobDescription.text = data.detail ?? ""
        self.lblStatus.text = "Completed"
        let comment = data.comments.first?.comment ?? "-"
        self.lblSupervisorComment.text = comment
        let rating = Double(data.comments.first?.rating ?? "0") ?? 0.0
        self.viewRating.rating = rating
        let date = data.startDate ?? "2019-04-05 12:04:00"//"yyyy-MM-dd hh:mm:ss"
        self.lblDate.text = Utility.stringDateFormatter(dateStr: date, dateFormat: "yyyy-MM-dd hh:mm:ss", formatteddate: "dd MMM yyyy hh:mm a")
    }
    
}
