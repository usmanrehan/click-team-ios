//
//  Duties.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 04/03/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import ObjectMapper

enum DutyStatus{
    case inProgress
    case history
}

class Duties: BaseController {
    
    @IBOutlet weak var btnInProgress: BlueWhiteButton!
    @IBOutlet weak var btnHistory: BlueWhiteButton!
    @IBOutlet weak var tableView: UITableView!
    
    var dutyStatus = DutyStatus.inProgress
    var arrInprogressJobs = [JobModel]()
    var arrHistoryJobs = [JobModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Duties"
        self.registerCell()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        switch self.dutyStatus{
        case .inProgress:
            self.getInProgressDuties()
        case .history:
            self.getInCompletedDuties()
        }
    }
    
    @IBAction func onBtnInProgress(_ sender: BlueWhiteButton) {
        if self.btnInProgress.isSelected{return}
        self.btnInProgress.isSelected = true
        self.btnHistory.isSelected = false
        self.dutyStatus = .inProgress
        self.getInProgressDuties()
    }
    @IBAction func onBtnHistory(_ sender: BlueWhiteButton) {
        if self.btnHistory.isSelected{return}
        self.btnInProgress.isSelected = false
        self.btnHistory.isSelected = true
        self.dutyStatus = .history
        self.getInCompletedDuties()
    }
    
}
//MARK:- Helper Methods
extension Duties{
    private func registerCell(){
        self.tableView.register(UINib(nibName: "DutyInprogressTVC", bundle: nil), forCellReuseIdentifier: "DutyInprogressTVC")
        self.tableView.register(UINib(nibName: "DutyHistoryTVC", bundle: nil), forCellReuseIdentifier: "DutyHistoryTVC")
        
    }
    private func pushToJobDetail(index:Int){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "JobDetail") as! JobDetail
        controller.currentJob = self.arrInprogressJobs[index]
        controller.jobDetailType = .detail
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
extension Duties:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.dutyStatus{
        case .inProgress:
            return self.arrInprogressJobs.count
        case .history:
            return self.arrHistoryJobs.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch self.dutyStatus {
        case .inProgress:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DutyInprogressTVC", for: indexPath) as! DutyInprogressTVC
            cell.setData(data: self.arrInprogressJobs[indexPath.row])
            return cell
        case .history:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DutyHistoryTVC", for: indexPath) as! DutyHistoryTVC
            cell.setData(data: self.arrHistoryJobs[indexPath.row])
            return cell
        }
    }
}
extension Duties:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.dutyStatus == .inProgress{
            self.pushToJobDetail(index: indexPath.row)
        }
    }
}
//MARK:- Serivces
extension Duties{
    private func getInProgressDuties(){
        let type = "1"
        let status = "10"
        let params:[String:Any] = ["type":type,
                                   "status":status]
        APIManager.sharedInstance.usersAPIManager.GetAllJobs(params: params, success: { (responseObject) in
            guard let jobs = responseObject as? [[String : Any]] else {return}
            self.arrInprogressJobs = Mapper<JobModel>().mapArray(JSONArray: jobs)
            self.tableView.reloadData()
            if self.arrInprogressJobs.isEmpty{
                Utility.main.showToast(message: "No in-progress jobs available")
            }
        }) { (error) in
            print(error)
        }
    }
    private func getInCompletedDuties(){
        let type = "1"
        let status = "40"
        let params:[String:Any] = ["type":type,
                                   "status":status]
        APIManager.sharedInstance.usersAPIManager.GetAllJobs(params: params, success: { (responseObject) in
            guard let jobs = responseObject as? [[String : Any]] else {return}
            self.arrHistoryJobs = Mapper<JobModel>().mapArray(JSONArray: jobs)
            self.tableView.reloadData()
            if self.arrHistoryJobs.isEmpty{
                Utility.main.showToast(message: "No completed jobs available")
            }
        }) { (error) in
            print(error)
        }
    }
}
