//
//  Company.swift
//  ClickTeam_iOS
//
//  Created by Syed Hassan Shah on 03/03/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import ObjectMapper

enum SelectionType:Int{
    case Training = 0//training
    case Departments = 1//departments
    case Policy = 2//policy
    case AbsoluteRules = 3//absolute_rules
    case Partners = 4//partners
    //aboutus
}

class Company: BaseController {
    
    var arrCMS = [CMSModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Click Team"
        self.getCMS()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnPressed(_ sender: UIButton) {
        switch sender.tag {
        case SelectionType.Training.rawValue:
            self.pushToCMS(selectionType: SelectionType.Training)
        case SelectionType.Departments.rawValue:
            self.pushToCMS(selectionType: SelectionType.Departments)
        case SelectionType.Policy.rawValue:
            self.pushToCMS(selectionType: SelectionType.Policy)
        case SelectionType.AbsoluteRules.rawValue:
            self.pushToCMS(selectionType: SelectionType.AbsoluteRules)
        case SelectionType.Partners.rawValue:
            self.pushToCMS(selectionType: SelectionType.Partners)
        default:
            break
        }
  
    }
}
//MARK:- Helper Methods
extension Company{
    private func pushToCMS(selectionType:SelectionType){
        let storyboard = AppStoryboard.Home.instance
        guard let controller = storyboard.instantiateViewController(withIdentifier: "CMS") as? CMS else {return}
        controller.selectionType = selectionType
        controller.content = self.getSelectedContent(selectionType: selectionType)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    private func getSelectedContent(selectionType: SelectionType)-> CMSModel{
        switch selectionType{
        case .Training:
            for item in self.arrCMS{
                if (item.slug ?? "") == "training"{
                    return item
                }
            }
        case .Departments:
            for item in self.arrCMS{
                if (item.slug ?? "") == "departments"{
                    return item
                }
            }
        case .Policy:
            for item in self.arrCMS{
                if (item.slug ?? "") == "policy"{
                    return item
                }
            }
        case .AbsoluteRules:
            for item in self.arrCMS{
                if (item.slug ?? "") == "absolute_rules"{
                    return item
                }
            }
        case .Partners:
            for item in self.arrCMS{
                if (item.slug ?? "") == "partners"{
                    return item
                }
            }
        }
        return CMSModel()
    }
}
//MARK:- Services
extension Company{
    private func getCMS(){
        APIManager.sharedInstance.usersAPIManager.getCMS(params: [:], success: { (responseObject) in
            guard let jobs = responseObject as? [[String : Any]] else {return}
            self.arrCMS = Mapper<CMSModel>().mapArray(JSONArray: jobs)
        }) { (error) in
            print(error.localizedDescription)
        }
    }
}
