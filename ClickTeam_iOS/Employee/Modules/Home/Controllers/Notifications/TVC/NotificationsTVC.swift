//
//  NotificationsTVC.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 19/05/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit

class NotificationsTVC: UITableViewCell {

    @IBOutlet weak var lblNotification: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    func setData(data:NotificationModel){
        self.lblNotification.text = data.message ?? "-"
        self.lblDate.text = data.createdAt ?? "-"
    }

}
