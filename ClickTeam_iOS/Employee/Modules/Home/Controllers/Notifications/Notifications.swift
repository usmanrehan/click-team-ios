//
//  Notifications.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 19/05/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import ObjectMapper

class Notifications: BaseController {

    @IBOutlet weak var tableView: UITableView!
    var arrNotifications = [NotificationModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Notifications"
        self.getNotifications()
        // Do any additional setup after loading the view.
    }

}
extension Notifications: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrNotifications.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationsTVC", for: indexPath) as! NotificationsTVC
        let data = self.arrNotifications[indexPath.row]
        cell.setData(data: data)
        return cell
    }
}
extension Notifications{
    private func getNotifications(){
        let ref_id = AppStateManager.sharedInstance.loggedInUser.id
        let params:[String:Any] = ["search":"ref_id:\(ref_id)"]
        APIManager.sharedInstance.usersAPIManager.GetNotifications(params: params, success: { (responseObject) in
            print(responseObject)
            guard let notifications = responseObject as? [[String : Any]] else {return}
            self.arrNotifications = Mapper<NotificationModel>().mapArray(JSONArray: notifications)
            self.tableView.reloadData()
            if self.arrNotifications.isEmpty{
                Utility.main.showToast(message: "No notifications available")
            }
        }) { (error) in
            print(error.localizedDescription)
        }
    }
}
