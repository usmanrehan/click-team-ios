//
//  SlideShow.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 22/06/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import ImageSlideshow
import SDWebImage

class SlideShow: BaseController {

    @IBOutlet weak var viewSlideShow: ImageSlideshow!
    
    var complain: ComplainModel?
    var currentJob: JobModel?
    var arrImages = [String]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.complain != nil{
            self.title = "Complain # \(self.complain?.id ?? 0)"
        }
        if self.currentJob != nil{
            self.title = "Job # \(self.currentJob?.id ?? "0")"
        }
        self.setSlideShow()
    }
    
    private func setSlideShow(){
        self.viewSlideShow.zoomEnabled = true
        var arrSDWebImageSource = [SDWebImageSource]()
        for imageUrlString in self.arrImages{
            guard let img = SDWebImageSource(urlString: imageUrlString, placeholder: UIImage(named: "placeholderimage")) else {return}
            arrSDWebImageSource.append(img)
        }
        self.viewSlideShow.setImageInputs(arrSDWebImageSource)
    }
}
