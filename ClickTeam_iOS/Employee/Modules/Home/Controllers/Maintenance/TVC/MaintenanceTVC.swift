//
//  DutiesTVC.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 04/03/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit

class MaintenanceTVC: UITableViewCell {

    @IBOutlet weak var lblJobTitle: UILabel!
    @IBOutlet weak var lblJobDescription: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblAssignedBy: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    func setData(data:JobModel){
        self.lblJobTitle.text = data.title ?? "-"
        self.lblJobDescription.text = data.detail ?? "-"
        self.lblStatus.text = "Inprogress"
        self.lblAssignedBy.text = data.createdBy ?? "-"
        let dateStr = data.createdAt ?? "2019-04-05 12:04:00"
        let date = Utility.stringDateFormatter(dateStr: dateStr, dateFormat: "yyyy-MM-dd hh:mm:ss", formatteddate: "dd - MMM - yyyy")
        self.lblDate.text = "Message On: \(date)"
    }
    
}
