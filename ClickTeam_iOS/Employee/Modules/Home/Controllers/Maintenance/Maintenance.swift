//
//  Maintenance.swift
//  ClickTeam_iOS
//
//  Created by M Usman Bin Rehan on 16/05/2019.
//  Copyright © 2019 Click Team. All rights reserved.
//

import UIKit
import ObjectMapper

class Maintenance: BaseController {

    @IBOutlet weak var tableView: UITableView!
    
    var arrMaintenanceInProgress = [JobModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Maintenance"
        self.registerCell()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getMaintenanceTasks()
    }
}
extension Maintenance{
    private func registerCell(){
        self.tableView.register(UINib(nibName: "MaintenanceTVC", bundle: nil), forCellReuseIdentifier: "MaintenanceTVC")
    }
    private func pushToJobDetail(index:Int){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "JobDetail") as! JobDetail
        controller.currentJob = self.arrMaintenanceInProgress[index]
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
extension Maintenance:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrMaintenanceInProgress.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MaintenanceTVC", for: indexPath) as! MaintenanceTVC
        let data = self.arrMaintenanceInProgress[indexPath.row]
        cell.setData(data: data)
        return cell
    }
}
extension Maintenance:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.pushToJobDetail(index: indexPath.row)
    }
}
//MARK:- Services
extension Maintenance{
    private func getMaintenanceTasks(){
        self.arrMaintenanceInProgress.removeAll()
        self.tableView.reloadData()
        let type = "2"
        let status = "10"
        let user_id = AppStateManager.sharedInstance.loggedInUser.id
        let params:[String:Any] = ["user_id":user_id,"type":type,"status":status]
        APIManager.sharedInstance.usersAPIManager.GetAllJobs(params: params, success: { (responseObject) in
            guard let maintenanceInProgress = responseObject as? [[String : Any]] else {return}
            self.arrMaintenanceInProgress = Mapper<JobModel>().mapArray(JSONArray: maintenanceInProgress)
            self.tableView.reloadData()
            if self.arrMaintenanceInProgress.isEmpty{
                Utility.main.showToast(message: "No maintenance tasks available")
            }
        }) { (error) in
            print(error)
        }
    }
}
